import { initializeAllButtonListeners } from './controllers/buttons/initializeButtons';
import { setAuthOnStatusChange } from './controllers/auth/auth';
import { setMealsDatabaseListener } from './controllers/database/listeners/mealsDatabaseListener';
import { setActivitiesDatabaseListener } from './controllers/database/listeners/activitiesDatabaseListener';
import { loadPage } from './controllers/loadPage';
import { autocomplete } from './views/article/autocompleteSearch';
import { addPasswordsCheckboxesListeners } from './views/article/displayPasswords';

// add button listeners
initializeAllButtonListeners();

// add checkboxes listeners
addPasswordsCheckboxesListeners();

// add auth listeners
setAuthOnStatusChange();

// add database listeners
setMealsDatabaseListener();
setActivitiesDatabaseListener();

// set autocomplete listeners
autocomplete('header-search', 'searched-records');

// set window.onload function
window.onload = loadPage;
