import { validateNumberInput } from './validateNumberInput';
import { validateTextInput } from './validateTextInput';

/**
 * Function runs validateNumberInput and validateTextInput functions on both
 * number and text inputs.
 *
 * @param numberInputs Array of number input elements ids.
 * @param textInputs Array of text input elements ids.
 * @returns {boolean} Boolean containing information whether both validations
 * passed successfully or not.
 */
export function validate(numberInputs, textInputs) {
  let success = true;
  for (let id of numberInputs) {
    if (!validateNumberInput(id)) {
      success = false;
    }
  }
  for (let tid of textInputs) {
    if (!validateTextInput(tid)) {
      success = false;
    }
  }
  return success;
}
