/**
 * Validates text input on being non-empty and up to 50 chars long.
 * Eventually input turns red.
 * @param inputElementId Id HTML tag of input element.
 * @returns {boolean} Validation result (true - passed, false - not passed)
 */
export function validateTextInput(inputElementId) {
  let element = document.getElementById(inputElementId);
  if (element.value === '' || element.value.length > 50) {
    element.classList.add('error');
    return false;
  } else if (element.classList.contains('error')) {
    element.classList.remove('error');
    return true;
  }
  return true;
}
