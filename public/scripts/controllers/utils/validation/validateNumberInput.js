/**
 * Function validates number input on being non-negative, non-empty and number values.
 * Eventually adds error class to invalidated inputs.
 * @param inputElementId Id HTML tag of input element.
 * @returns {boolean} Validation result (true - passed, false - not passed)
 */
export function validateNumberInput(inputElementId) {
  let element = document.getElementById(inputElementId);
  if (element.value < 0 || element.value === '' || isNaN(element.value)) {
    element.classList.add('error');
    return false;
  } else if (element.classList.contains('error')) {
    element.classList.remove('error');
    return true;
  }
  return true;
}
