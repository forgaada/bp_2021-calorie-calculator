import { todayDate } from '../utils/variables';
import { clearContentBoxes } from '../views/article/contentBoxes';
import { clearNewMealPage } from '../views/article/clearNewMealPage';
import { setCheckboxesChecked } from '../views/setCheckboxes';
import { pageController } from './pageController';
import { updateOnlineStatus } from './updateOnlineStatus';

/**
 * Function registers Service Worker script and initializes today variable to
 * today's date in 'dd/mm/yyyy' format.
 * Loads stats and meals variables and adds eventListener to 'popstate' event
 * for changing page and loads home page.
 */
export function loadPage() {
  // register sw.js script
  navigator.serviceWorker.register('/sw.js').then(
    function (registration) {
      console.log(
        'ServiceWorker registration successful with scope: ',
        registration.scope,
      );
    },
    function (err) {
      console.log('ServiceWorker registration failed: ', err);
    },
  );
  // load current date
  let dd = String(todayDate.getDate()).padStart(2, '0');
  let mm = String(todayDate.getMonth() + 1).padStart(2, '0');
  let yyyy = todayDate.getFullYear();
  today = dd + '/' + mm + '/' + yyyy;
  // clear all content boxes
  clearContentBoxes();
  // clear new meal inputs
  clearNewMealPage();
  // set checkboxes
  setCheckboxesChecked();
  // set page location hash
  window.addEventListener('popstate', () => {
    pageController(window.location.hash);
  });
  // set online status listener
  window.addEventListener('online', updateOnlineStatus);
  window.addEventListener('offline', updateOnlineStatus);
  // change page location
  window.location.hash = '#state';
}
