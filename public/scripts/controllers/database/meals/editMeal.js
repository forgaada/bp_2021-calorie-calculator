/**
 * Function updates meal record in database.
 * @param mealId Id of meal created by Firestore.
 * @param name Name of meal created by author.
 * @param calories Meal calories value set by author.
 * @param protein Meal protein value set by author.
 * @param fat Meal fats value set by author.
 * @param sugars Meal sugars value set by author.
 * @param ean Meal EAN code value set by author.
 */
export async function editMeal(mealId, name, calories, protein, fat, sugars, ean) {
  try {
    let response = await db
      .collection('meals')
      .doc(mealId)
      .update({
        name: name,
        calories: parseFloat(calories),
        protein: parseFloat(protein),
        fat: parseFloat(fat),
        sugars: parseFloat(sugars),
        ean: parseInt(ean),
      });
  } catch (error) {
    console.log('Error updating meal: ', error);
  }
}
