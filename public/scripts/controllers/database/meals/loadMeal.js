export async function loadMealDataOnce(createdMealId) {
  try {
    return await db.collection('meals').doc(createdMealId).get();
  } catch (error) {
    console.log('Error occurred during loading meals: ' + error);
  }
}
