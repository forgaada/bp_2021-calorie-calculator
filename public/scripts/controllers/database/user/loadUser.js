import { User } from '../../../models/User';
import { displayDropdown } from '../../../views/header/dropdown';
import { updateUserBox } from '../../../views/navigation/userBox';
import { getParams } from '../../utils/getParams';
import { loadCreatedMealPage } from '../../meals/createdMealPageController';
import { loadStatePage } from '../../state/statePageController';

/**
 * Function loads user data from database and updates user snapshot.
 * @returns {Promise<void>}
 */
export async function loadUserDataOnce() {
  try {
    const response = await db.collection('users').doc(userAuth.uid).get();
    userSnapshot = response.data();
    _user = new User(
      userSnapshot.name,
      userSnapshot.weight,
      userSnapshot.height,
      userSnapshot.year_of_birth,
      userSnapshot.sex,
      userSnapshot.activity,
      userSnapshot.goal,
      userSnapshot.eaten_meals,
      userSnapshot.performed_activities,
    );
    displayDropdown();
    updateUserBox();
    let params = getParams(window.location.href);
    if (params.meal_id != null) {
      loadCreatedMealPage(params.meal_id);
    } else {
      window.location.hash = '#state';
      loadStatePage();
    }
  } catch (error) {
    console.log('Error occurred when loading user data: ' + error);
    window.location.hash = '#logout';
  }
}
