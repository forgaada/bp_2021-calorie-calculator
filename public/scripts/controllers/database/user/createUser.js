import { signUpForm } from '../../registration/signUpFormController';

export async function createUser(cred) {
  try {
    const response = await db.collection('users').doc(cred.user.uid).set({
      name: signUpForm['reg_name'].value,
      weight: 80.0,
      height: 175.0,
      year_of_birth: 1990.0,
      sex: 'male',
      activity: 1.0,
      goal: 1.0,
      eaten_meals: [],
      performed_activities: [],
    });

    // clear sign up form data
    signUpForm['reg_mail'].value = '';
    signUpForm['reg_password'].value = '';
    signUpForm['reg_name'].value = '';

    console.log('User successfully registered!');
  } catch (error) {
    console.log('Error occurred during creating new user: ' + error);
  }
}
