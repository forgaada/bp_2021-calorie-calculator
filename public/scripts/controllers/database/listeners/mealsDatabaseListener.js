/**
 * Function sets listener on 'meals' collection in Firestore. When collection
 * is updated 'onSnapshot' function is triggered and updates mealsSnapshot.
 */
export function setMealsDatabaseListener() {
  db.collection('meals').onSnapshot(
    function (querySnapshot) {
      mealsSnapshot = [];
      querySnapshot.forEach(function (doc) {
        mealsSnapshot.push(doc);
      });
    },
    (error) => {
      console.log(error);
    },
  );
}
