/**
 * Function sets listener on 'activities' collection in Firestore. When collection
 * is updated 'onSnapshot' function is triggered and updates activitiesSnapshot.
 */
export function setActivitiesDatabaseListener() {
  db.collection('activities').onSnapshot(
    function (querySnapshot) {
      activitiesSnapshot = [];
      querySnapshot.forEach(function (doc) {
        activitiesSnapshot.push(doc);
      });
    },
    (error) => {
      console.log(error);
    },
  );
}
