import { validate } from '../utils/validation/validate';
import { renderSearchedActivity } from '../../views/article/renderSearchedActivity';
import { RESULTS_NUM } from '../../utils/variables';

export const search_activity_button = document.getElementById('search-activity-button');

/**
 * Function adds listener on button for searching an activity.
 *
 * Button's onclick function searches for results in database snapshot using fulltext
 * search. Results are then displayed on pages with fixed number of results.
 */
export function addSearchActivityButtonListener() {
  search_activity_button.addEventListener('click', function (e) {
    let searchedText = document.getElementById('search-activity-input').value;
    let textElementsToValidate = ['search-activity-input'];
    if (!validate([], textElementsToValidate)) {
      return;
    }
    let selectElement = document.getElementById('activity_search_type');
    let searchType = selectElement.options[selectElement.selectedIndex].text;

    // get activities from db snapshot
    let activities = [];

    for (let doc of activitiesSnapshot) {
      if (searchType === 'Názvu') {
        const reg = new RegExp(searchedText, 'i');
        if (doc.data().name.match(reg)) {
          activities.push({ id: doc.id, data: doc.data() });
        }
      } else if (searchType === 'Tagu') {
        let reg = new RegExp(searchedText, 'i');
        for (let tag of doc.data().tags) {
          if (tag.match(reg)) {
            activities.push({ id: doc.id, data: doc.data() });
            break;
          }
        }
      }
    }

    // render activity pages
    let currentPage = 0;

    document.getElementById(
      'search-activity-content-all',
    ).innerHTML = `Výsledky hledání: "${searchedText}"`;

    let pagesCount = parseInt(activities.length / RESULTS_NUM);
    if (activities.length % RESULTS_NUM > 0) {
      pagesCount++;
    }

    let createdPagesDiv = document.createElement('div');
    createdPagesDiv.classList.add('content_inline_center');

    let createdPages = [];

    for (let i = 1; i <= pagesCount; i++) {
      let createdPage = document.createElement('label');
      createdPage.innerText = i.toString();
      createdPages.push(createdPage);
      createdPage.style.cursor = 'pointer';
      createdPage.addEventListener('click', () => {
        for (let page of createdPages) {
          if (page.innerText === createdPage.innerText) {
            page.style.fontSize = '18px';
          } else {
            page.style.fontSize = '14px';
          }
        }
        document.getElementById(
          'search-activity-content-all',
        ).innerHTML = `Výsledky hledání: "${searchedText}"`;
        document
          .getElementById('search-activity-content-all')
          .appendChild(createdPagesDiv);
        currentPage = parseInt(createdPage.innerText) - 1;
        // render current page
        for (
          let i = currentPage * RESULTS_NUM;
          i < RESULTS_NUM + currentPage * RESULTS_NUM;
          i++
        ) {
          if (i < activities.length) {
            renderSearchedActivity(activities[i].id, activities[i].data);
          }
        }
      });
      createdPagesDiv.appendChild(createdPage);
    }

    document.getElementById('search-activity-content-all').appendChild(createdPagesDiv);

    // render first page
    for (
      let i = currentPage * RESULTS_NUM;
      i < RESULTS_NUM + currentPage * RESULTS_NUM;
      i++
    ) {
      if (i < activities.length) {
        renderSearchedActivity(activities[i].id, activities[i].data);
      }
    }

    // show current page
    for (let page of createdPages) {
      if (page.innerText === '1') {
        page.style.fontSize = '18px';
      } else {
        page.style.fontSize = '14px';
      }
    }

    document.getElementById('search-activity-input').value = ``;
  });
}
