import { clearNewMealPage } from '../../views/article/clearNewMealPage';

export const new_meal_cancel_button = document.getElementById('new_meal_cancel_button');

/**
 * Function adds listener to button for clearing 'create new meal' form.
 */
export function addNewMealCancelButtonEventListener() {
  new_meal_cancel_button.addEventListener('click', () => {
    clearNewMealPage();
  });
}
