import { validate } from '../utils/validation/validate';
import { User } from '../../models/User';
import { updateUserBox } from '../../views/navigation/userBox';

export const basal_button = document.getElementById('basal_button');

/**
 * Function adds listener on button for updating user settings.
 *
 * Button's onclick function saves user's information to Firestore and loads
 * state page.
 */
export function addSettingsButtonEventListener() {
  basal_button.addEventListener('click', function (e) {
    e.preventDefault();
    let textElementsToValidate = ['name-input'];
    let numberElementsToValidate = [
      'weight-input',
      'height-input',
      'year-of-birth-input',
    ];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }
    _user = new User(
      document.getElementById('name-input').value,
      document.getElementById('weight-input').value,
      document.getElementById('height-input').value,
      document.getElementById('year-of-birth-input').value,
      document.querySelector('.sex:checked').value,
      document.querySelector('.activity:checked').value,
      document.querySelector('.goal:checked').value,
      _user._eaten_meals,
      _user._performed_activities,
    );
    _user.saveUser();
    updateUserBox();
    window.location.hash = '#state';
  });
}
