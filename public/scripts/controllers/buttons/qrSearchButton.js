import { scanning_start_button } from './scanningStartButton';

export const qr_search_button = document.getElementById('qr-code-search-logo');

/**
 * Function adds listener on button for searching with QR scanner.
 * Button's onclick function redirects user.
 */
export function addQrSearchButtonListener() {
  qr_search_button.addEventListener('click', function (e) {
    window.location.hash = 'meals';
    if (userAuth) {
      scanning_start_button.focus();
      scanning_start_button.scrollIntoView({
        behavior: 'smooth',
        block: 'end',
        inline: 'nearest',
      });
    }
  });
}
