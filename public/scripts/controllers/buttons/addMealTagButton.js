import { validate } from '../utils/validation/validate';

export const add_meal_tag_button = document.getElementById('add-meal-tag-button');

/**
 * Function adds listener on button for adding new tag to currently created meal.
 */
export function addMealTagButtonEventListener() {
  add_meal_tag_button.addEventListener('click', function (e) {
    let textElementsToValidate = ['add-meal-tag'];
    if (!validate([], textElementsToValidate)) {
      return;
    }

    let createdElement = document.createElement('label');
    createdElement.classList.add('tag');
    createdElement.innerText = document.getElementById('add-meal-tag').value;
    document.getElementById('add-meal-tag').value = '';
    document.querySelector('#added-tags').appendChild(createdElement);

    createdElement.addEventListener('click', () => {
      document.querySelector('#added-tags').removeChild(createdElement);
    });
  });
}
