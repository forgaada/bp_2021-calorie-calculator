import {
  isScanning,
  startScanning,
  stopScanning,
} from '../../qr_code_scanner/qr-code-scanner';

export const scanning_start_button = document.getElementById('scanning-start-button');

/**
 * Function adds listener on QR code scanning button.
 */
export function addScanningStartButtonListener() {
  scanning_start_button.addEventListener('click', function () {
    if (!isScanning) {
      startScanning();
      scanning_start_button.innerText = 'Stop';
      scanning_start_button.style.backgroundColor = 'red';
    } else {
      stopScanning();
      scanning_start_button.innerText = 'Start';
      scanning_start_button.style.backgroundColor = '#2fd015';
    }
  });
}
