export const add_existing_meal_button = document.getElementById('add-existing-meal');

/**
 * Function adds listener on button for adding existing meal to newly created.
 */
export function addAddExistingMealButtonListener() {
  add_existing_meal_button.addEventListener('click', () => {
    window.location.hash = '#meals';
    document
      .getElementById('search-meal-input')
      .scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    document.getElementById('search-meal-input').focus({ preventScroll: false });
  });
}
