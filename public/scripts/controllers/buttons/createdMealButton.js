import { validate } from '../utils/validation/validate';
import { getUniqueID } from '../utils/getId';

/**
 * Function adds listener on button for adding new meal record to user's meal records.
 */
export function addCreatedMealButtonEventListener() {
  let divElement = document.getElementById('created-meal-buttons');

  let createdMealButton = document.createElement('button');
  createdMealButton.classList.add('content-button');
  createdMealButton.innerText = 'Zapsat';
  createdMealButton.id = 'record-meal-button';

  createdMealButton.addEventListener('click', function (e) {
    let textElementsToValidate = [
      'created-meal-name-input',
      'created-meal-calories-input',
      'created-meal-protein-input',
      'created-meal-fat-input',
      'created-meal-sugars-input',
    ];
    let numberElementsToValidate = ['created-meal-amount-input'];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }

    let selectElement = document.getElementById('meal-type');
    let mealType = selectElement.options[selectElement.selectedIndex].text;

    let mealRecord = {
      meal_id: document.getElementById('created-meal-id-input').value,
      amount: parseFloat(document.getElementById('created-meal-amount-input').value),
      meal_type: mealType,
      meal_record_id: getUniqueID(),
      date: today,
    };
    document.getElementById('created-meal-amount-input').value = ``;
    _user._eaten_meals.push(mealRecord);
    _user.saveUser();
    window.location.hash = '#state';
  });

  divElement.appendChild(createdMealButton);
}
