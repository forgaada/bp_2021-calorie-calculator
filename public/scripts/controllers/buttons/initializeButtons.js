import { addActivityButtonEventListener } from './activityButton';
import { addLoginFormEventListener } from '../login/loginFormController';
import { addSignUpFormEventListener } from '../registration/signUpFormController';
import { addNewActivityTagButtonEventListener } from './addActivityTagButton';
import { addNewMealButtonListener } from './newMealButton';
import { addSearchActivityButtonListener } from './searchActivityButton';
import { addSearchMealButtonEventListener } from './searchMealButton';
import { addSettingsButtonEventListener } from './settingsButton';
import { addMealTagButtonEventListener } from './addMealTagButton';
import { addScanningStartButtonListener } from './scanningStartButton';
import { addQrSearchButtonListener } from './qrSearchButton';
import { addNewMealCancelButtonEventListener } from './newMealCancelButton';
import { addAddExistingMealButtonListener } from './addExistingMealButton';

/**
 * Function initializes all static buttons with event listeners in application.
 */
export function initializeAllButtonListeners() {
  addLoginFormEventListener();
  addSignUpFormEventListener();
  addActivityButtonEventListener();
  addNewActivityTagButtonEventListener();
  addNewMealButtonListener();
  addSearchActivityButtonListener();
  addSearchMealButtonEventListener();
  addSettingsButtonEventListener();
  addMealTagButtonEventListener();
  addScanningStartButtonListener();
  addQrSearchButtonListener();
  addNewMealCancelButtonEventListener();
  addAddExistingMealButtonListener();
}
