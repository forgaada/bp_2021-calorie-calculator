import { validate } from '../utils/validation/validate';
import { Activity } from '../../models/Activity';

export const activity_button = document.getElementById('activity_button');

/**
 * Function adds listener on button for adding new activity record to Firestore.
 */
export function addActivityButtonEventListener() {
  activity_button.addEventListener('click', function (e) {
    e.preventDefault();

    let textElementsToValidate = ['activity-name-input'];
    let numberElementsToValidate = ['activity-met-index-input'];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }
    let tags = [];
    for (let element of document.querySelector('#activity-tags').childNodes) {
      tags.push(element.textContent);
    }
    let newActivity = new Activity(
      document.getElementById('activity-name-input').value,
      document.getElementById('activity-met-index-input').value,
      tags,
      userAuth.uid,
    );
    newActivity.saveActivity();
    document.getElementById('activity-name-input').value = '';
    document.getElementById('activity-met-index-input').value = '';

    document.querySelector('#activity-tags').innerHTML = ``;
  });
}
