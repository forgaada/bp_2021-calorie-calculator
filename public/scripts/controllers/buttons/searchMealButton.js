import { validate } from '../utils/validation/validate';
import { renderSearchedMeal } from '../../views/article/renderSearchedMeal';
import { RESULTS_NUM } from '../../utils/variables';

export const search_meal_button = document.getElementById('search-meal-button');

/**
 * Function adds listener on button for searching a meal in Firestore.
 *
 * Button's onclick function searches for results in database snapshot using fulltext
 * search. Results of searched meals with brief description are then displayed
 * on pages with fixed number of results.
 */
export function addSearchMealButtonEventListener() {
  search_meal_button.addEventListener('click', function (e) {
    let reg;
    let searchedText = document.getElementById('search-meal-input').value;
    let textElementsToValidate = ['search-meal-input'];
    if (!validate([], textElementsToValidate)) {
      return;
    }
    let selectElement = document.getElementById('search_type');
    let searchType = selectElement.options[selectElement.selectedIndex].text;

    // get meals from db snapshots
    let meals = [];

    if (searchType === 'Názvu') {
      for (let doc of mealsSnapshot) {
        reg = new RegExp(searchedText, 'i');
        if (doc.data().name.match(reg)) {
          meals.push({ id: doc.id, data: doc.data() });
        }
      }
    } else if (searchType === 'Tagu') {
      for (let doc of mealsSnapshot) {
        reg = new RegExp(searchedText, 'i');
        for (let tag of doc.data().tags) {
          if (tag.match(reg)) {
            meals.push({ id: doc.id, data: doc.data() });
            break;
          }
        }
      }
    } else if (searchType === 'Eanu') {
      for (let doc of mealsSnapshot) {
        if (doc.data().ean === parseInt(searchedText)) {
          meals.push({ id: doc.id, data: doc.data() });
        }
      }
    }

    // render meal pages
    let currentPage = 0;

    document.getElementById(
      'search-meal-content-all',
    ).innerHTML = `Výsledky hledání: "${searchedText}"`;

    let pagesCount = parseInt(meals.length / RESULTS_NUM);
    if (meals.length % RESULTS_NUM > 0) {
      pagesCount++;
    }

    let createdPagesDiv = document.createElement('div');
    createdPagesDiv.classList.add('content_inline_center');

    let createdPages = [];

    for (let i = 1; i <= pagesCount; i++) {
      let createdPage = document.createElement('label');
      createdPage.innerText = i.toString();
      createdPages.push(createdPage);
      createdPage.style.cursor = 'pointer';
      createdPage.addEventListener('click', () => {
        for (let page of createdPages) {
          if (page.innerText === createdPage.innerText) {
            page.style.fontSize = '18px';
          } else {
            page.style.fontSize = '14px';
          }
        }
        document.getElementById(
          'search-meal-content-all',
        ).innerHTML = `Výsledky hledání: "${searchedText}"`;
        document.getElementById('search-meal-content-all').appendChild(createdPagesDiv);
        currentPage = parseInt(createdPage.innerText) - 1;
        // render current page
        for (
          let i = currentPage * RESULTS_NUM;
          i < RESULTS_NUM + currentPage * RESULTS_NUM;
          i++
        ) {
          if (i < meals.length) {
            renderSearchedMeal(meals[i].id, meals[i].data);
          }
        }
      });
      createdPagesDiv.appendChild(createdPage);
    }

    document.getElementById('search-meal-content-all').appendChild(createdPagesDiv);

    // render first page
    for (
      let i = currentPage * RESULTS_NUM;
      i < RESULTS_NUM + currentPage * RESULTS_NUM;
      i++
    ) {
      if (i < meals.length) {
        renderSearchedMeal(meals[i].id, meals[i].data);
      }
    }

    // show current page
    for (let page of createdPages) {
      if (page.innerText === '1') {
        page.style.fontSize = '18px';
      } else {
        page.style.fontSize = '14px';
      }
    }

    document.getElementById('search-meal-input').value = ``;
  });
}
