import { validate } from '../utils/validation/validate';

export const add_activity_tag_button = document.getElementById('add-activity-tag-button');

/**
 * Function adds listener on button for adding new tag to created activity.
 */
export function addNewActivityTagButtonEventListener() {
  add_activity_tag_button.addEventListener('click', function (e) {
    let textElementsToValidate = ['add-activity-tag'];
    if (!validate([], textElementsToValidate)) {
      return;
    }

    let createdElement = document.createElement('label');
    createdElement.classList.add('tag');
    createdElement.innerText = document.getElementById('add-activity-tag').value;
    document.getElementById('add-activity-tag').value = '';
    document.querySelector('#activity-tags').appendChild(createdElement);

    createdElement.addEventListener('click', () => {
      document.querySelector('#activity-tags').removeChild(createdElement);
    });
  });
}
