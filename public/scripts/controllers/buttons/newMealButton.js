import { validate } from '../utils/validation/validate';
import { Meal } from '../../models/Meal';
import { clearNewMealPage } from '../../views/article/clearNewMealPage';

export const meal_button = document.getElementById('meal_button');

/**
 * Function adds listener on button for adding new meal to Firestore.
 */
export function addNewMealButtonListener() {
  // Adding event listeners to button elements.
  meal_button.addEventListener('click', function (e) {
    e.preventDefault();

    let textElementsToValidate = ['meal-name-input'];
    let numberElementsToValidate = [
      'meal-calories-input',
      'meal-protein-input',
      'meal-fat-input',
      'meal-sugars-input',
    ];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }
    let ean = 0;
    let ean_input = document.getElementById('meal-ean-input').value;
    if (ean_input) {
      ean = ean_input;
    }
    let tags = [];
    for (let element of document.querySelector('#added-tags').childNodes) {
      tags.push(element.textContent);
    }
    let selectElement = document.getElementById('amount');
    let amount = selectElement.options[selectElement.selectedIndex].text;
    let newMeal = new Meal(
      document.getElementById('meal-name-input').value,
      document.getElementById('meal-calories-input').value,
      document.getElementById('meal-protein-input').value,
      document.getElementById('meal-fat-input').value,
      document.getElementById('meal-sugars-input').value,
      ean,
      tags,
      amount,
      userAuth.uid,
    );
    newMeal.saveMeal();
    clearNewMealPage();
  });
}
