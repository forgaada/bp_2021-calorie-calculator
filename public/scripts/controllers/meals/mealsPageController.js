import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { updateArticle } from '../../views/updateArticle';
import { pageLogoImg } from '../../utils/variables';
import { scanning_start_button } from '../buttons/scanningStartButton';
import { highlightLink } from '../../views/navigation/linkHighlight';

/**
 * Function loads meals page for searching meals and updates content accordingly
 */
export function loadMealsPage() {
  clearContentBoxes();
  highlightLink('meals-link');
  loadContentBoxes('meals-boxes');
  updateArticle(
    'Hledání a zápis potravin',
    'Vyhledaná jídla si lze zapsat do příjmu, nebo přidat k nově vytvářenému pokrmu ze sekce Nové jídlo.',
  );
  pageLogoImg.src = 'images/search.png';
  document.getElementById('search-meal-content-all').innerHTML = 'Výsledky hledání: ';
  scanning_start_button.innerText = 'Start';
  scanning_start_button.style.backgroundColor = '#2fd015';
}
