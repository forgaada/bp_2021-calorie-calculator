/**
 * Function adds values to currently created meal. Values are arithmetically
 * averaged with previously added meals.
 * @param meal Object containing information about meal.
 * @param amount Amount of added meal (on 1g/ml or 100g/ml).
 */
export function addToNewMeal(meal, amount) {
  let amountCoefficient = parseFloat(amount);
  let caloriesValue = 0;
  let proteinValue = 0;
  let fatValue = 0;
  let sugarsValue = 0;
  let totalAmount = 0;
  let newTotalAmount = 0;

  // update new total amount of meal
  if (
    !isNaN(document.getElementById('new-meal-total-amount').value) &&
    document.getElementById('new-meal-total-amount').value !== ''
  ) {
    totalAmount = parseFloat(document.getElementById('new-meal-total-amount').value);
  }
  newTotalAmount = totalAmount + parseFloat(amount);
  document.getElementById('new-meal-total-amount').value = newTotalAmount.toFixed(0);

  if (meal.amount === '100g' || meal.amount === '100ml') {
    amountCoefficient = parseFloat(amount) / 100;
  }
  if (
    !isNaN(document.getElementById('meal-calories-input').value) &&
    document.getElementById('meal-calories-input').value !== ''
  ) {
    caloriesValue = parseFloat(document.getElementById('meal-calories-input').value);
  }
  if (
    !isNaN(document.getElementById('meal-protein-input').value) &&
    document.getElementById('meal-protein-input').value !== ''
  ) {
    proteinValue = parseFloat(document.getElementById('meal-protein-input').value);
  }
  if (
    !isNaN(document.getElementById('meal-fat-input').value) &&
    document.getElementById('meal-fat-input').value !== ''
  ) {
    fatValue = parseFloat(document.getElementById('meal-fat-input').value);
  }
  if (
    !isNaN(document.getElementById('meal-sugars-input').value) &&
    document.getElementById('meal-sugars-input').value !== ''
  ) {
    sugarsValue = parseFloat(document.getElementById('meal-sugars-input').value);
  }

  document.getElementById('meal-calories-input').value = (
    (caloriesValue * (totalAmount / 100) + amountCoefficient * meal.calories) /
    (newTotalAmount / 100)
  ).toFixed(0);
  document.getElementById('meal-protein-input').value = (
    (proteinValue * (totalAmount / 100) + amountCoefficient * meal.protein) /
    (newTotalAmount / 100)
  ).toFixed(0);
  document.getElementById('meal-fat-input').value = (
    (fatValue * (totalAmount / 100) + amountCoefficient * meal.fat) /
    (newTotalAmount / 100)
  ).toFixed(0);
  document.getElementById('meal-sugars-input').value = (
    (sugarsValue * (totalAmount / 100) + amountCoefficient * meal.sugars) /
    (newTotalAmount / 100)
  ).toFixed(0);

  // add meal name as a new tag
  let createdElement = document.createElement('label');
  createdElement.classList.add('tag');
  createdElement.innerText = meal.name;
  document.querySelector('#added-tags').appendChild(createdElement);

  createdElement.addEventListener('click', () => {
    document.querySelector('#added-tags').removeChild(createdElement);
  });
}
