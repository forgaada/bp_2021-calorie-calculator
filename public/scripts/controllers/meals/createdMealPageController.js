import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { updateArticle } from '../../views/updateArticle';
import { ADMIN_EMAIL, pageLogoImg } from '../../utils/variables';
import { loadMealDataOnce } from '../database/meals/loadMeal';
import { addCreatedMealButtonEventListener } from '../buttons/createdMealButton';
import { editMeal } from '../database/meals/editMeal';
import { deleteMealById } from '../database/meals/deleteMeal';

/**
 * Function loads page with created meal content boxes.
 * @param createdMealId Meal record id created by getId function.
 */
export function loadCreatedMealPage(createdMealId) {
  clearContentBoxes();
  loadContentBoxes('created-meal-boxes');
  updateArticle(
    'Přehled potraviny',
    'Zde je zobrazen přehled vybrané potraviny. Potravinu lze zapsat do svého denního příjmu. Autor ji může také editovat a smazat.',
  );
  document.getElementById('created-tags').innerHTML = '';
  pageLogoImg.src = 'images/food.png';

  // set page location hash
  window.location.hash = '#created_meal';

  // create QR code

  let element = document.getElementById('gr_code');
  element.innerHTML = ``;

  if (element.display === 'none') {
    element.display = 'inline';
  } else element.display = 'none';

  let qr = new QRCode(document.getElementById('gr_code'), {
    width: 175,
    height: 175,
  });

  let url = 'https://calory-calculator-13.web.app/?meal_id=' + createdMealId;

  qr.makeCode(url);

  // load meal data once
  loadMealDataOnce(createdMealId).then((response) => {
    let mealSnapshotData = response.data();
    document.getElementById('created-meal-name-input').value = mealSnapshotData.name;
    document.getElementById('created-meal-calories-input').value =
      mealSnapshotData.calories;
    document.getElementById('created-meal-protein-input').value =
      mealSnapshotData.protein;
    document.getElementById('created-meal-fat-input').value = mealSnapshotData.fat;
    document.getElementById('created-meal-sugars-input').value = mealSnapshotData.sugars;
    document.getElementById('created-meal-ean-input').value = mealSnapshotData.ean;
    document.getElementById('created-meal-amount-type-input').value =
      mealSnapshotData.amount;
    document.getElementById('created-meal-id-input').value = createdMealId;

    // Clear buttons
    let divElement = document.getElementById('created-meal-buttons');
    divElement.innerHTML = ``;

    // Add event listeners to created buttons
    addCreatedMealButtonEventListener();

    // Verify current user is author of created meal
    if (userAuth.uid === mealSnapshotData.author_id || userAuth.email === ADMIN_EMAIL) {
      // Set inputs readonly - false
      document.getElementById('created-meal-name-input').readOnly = false;
      document.getElementById('created-meal-calories-input').readOnly = false;
      document.getElementById('created-meal-protein-input').readOnly = false;
      document.getElementById('created-meal-fat-input').readOnly = false;
      document.getElementById('created-meal-sugars-input').readOnly = false;
      document.getElementById('created-meal-ean-input').readOnly = false;
      // Create edit and delete buttons
      let editMealButton = document.createElement('button');
      editMealButton.classList.add('content-button');
      editMealButton.innerText = 'Upravit';
      editMealButton.addEventListener('click', () => {
        editMeal(
          createdMealId,
          document.getElementById('created-meal-name-input').value,
          document.getElementById('created-meal-calories-input').value,
          document.getElementById('created-meal-protein-input').value,
          document.getElementById('created-meal-fat-input').value,
          document.getElementById('created-meal-sugars-input').value,
          document.getElementById('created-meal-ean-input').value,
        );
        window.location.hash = '#state';
      });

      let deleteMealButton = document.createElement('button');
      deleteMealButton.classList.add('content-button');
      deleteMealButton.innerText = 'Smazat';
      deleteMealButton.id = 'delete-meal-button';
      deleteMealButton.addEventListener('click', () => {
        deleteMealById(createdMealId);
        window.location.hash = '#state';
      });

      divElement.appendChild(editMealButton);
      divElement.appendChild(deleteMealButton);
    } else {
      // Set inputs readonly - true
      document.getElementById('created-meal-name-input').readOnly = true;
      document.getElementById('created-meal-calories-input').readOnly = true;
      document.getElementById('created-meal-protein-input').readOnly = true;
      document.getElementById('created-meal-fat-input').readOnly = true;
      document.getElementById('created-meal-sugars-input').readOnly = true;
      document.getElementById('created-meal-ean-input').readOnly = true;
    }

    for (let element of mealSnapshotData.tags) {
      let createdElement = document.createElement('label');
      createdElement.classList.add('tag');
      createdElement.innerText = element;
      createdElement.style.cursor = 'pointer';
      createdElement.addEventListener('click', () => {
        window.location = '#meals';
        document.getElementById('search-meal-input').value = element;
        document.getElementById('search_type').value = 'Tagu';
        document.getElementById('search-meal-button').click();
        document.getElementById('search-meal-button').scrollIntoView({
          behavior: 'smooth',
          block: 'end',
          inline: 'nearest',
        });
      });
      document.querySelector('#created-tags').appendChild(createdElement);
    }

    const url = new URL(window.location);
    window.history.pushState({}, document.title, url);
  });
}
