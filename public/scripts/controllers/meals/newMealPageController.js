import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { updateArticle } from '../../views/updateArticle';
import { pageLogoImg } from '../../utils/variables';
import { highlightLink } from '../../views/navigation/linkHighlight';

/**
 * Function loads new meal page and updates page content accordingly.
 */
export function loadNewMealPage() {
  clearContentBoxes();
  highlightLink('newmeal-link');
  loadContentBoxes('home-boxes');
  updateArticle(
    'Tvorba nového pokrmu',
    'Vyplň údaje o jídle a ulož jej do databáze aplikace. Po uložení bude vygenerován QR kód pro sdílení informací o pokrmu. ' +
      'K vytvoření nového jídla lze použít i již existující pokrmy.',
  );
  pageLogoImg.src = 'images/cake.png';
}
