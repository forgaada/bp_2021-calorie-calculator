import { loadActivityPage } from './activities/activityPageController';
import { loadLoginPage } from './login/loginPageController';
import { loadSignUpPage } from './registration/registrationPageController';
import { loadMealsPage } from './meals/mealsPageController';
import { loadStatePage } from './state/statePageController';
import { loadSettingsPage } from './settings/settingsPageController';
import { loadNewMealPage } from './meals/newMealPageController';
import { isScanning, stopScanning } from '../qr_code_scanner/qr-code-scanner';
import { highlightLink, resetLinksHighlights } from '../views/navigation/linkHighlight';

/**
 * Function loads page content accordingly to 'window.location.hash'.
 * @param hash Value of currently displayed page's hash.
 */
export function pageController(hash) {
  document.getElementById('menu-control-input').checked = false;
  resetLinksHighlights();
  if (userAuth) {
    // turn off scanning
    if (isScanning) stopScanning();
    // reset page position
    window.scrollTo(0, 0);
    // load page with given hash
    switch (hash) {
      case '#newmeal': {
        document.title = 'Nové jídlo';
        loadNewMealPage();
        break;
      }
      case '#activity': {
        loadActivityPage();
        document.title = 'Aktivity';
        break;
      }
      case '#meals': {
        loadMealsPage();
        document.title = 'Hledání potravin';
        break;
      }
      case '#settings': {
        loadSettingsPage();
        document.title = 'Nastavení uživatele';
        break;
      }
      case '#logout': {
        highlightLink('logout-link');
        auth.signOut();
        break;
      }
      case '#created_meal': {
        document.title = 'Přehled potraviny';
        break;
      }
      case '#state': {
        loadStatePage();
        document.title = 'Denní přehled';
        break;
      }
    }
  } else {
    switch (hash) {
      case '#signup': {
        loadSignUpPage();
        document.title = 'Registrace';
        break;
      }
      default:
        loadLoginPage();
        document.title = 'Přihlášení';
        break;
    }
  }
}
