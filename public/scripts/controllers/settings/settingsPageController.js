import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { updateArticle } from '../../views/updateArticle';
import { pageLogoImg } from '../../utils/variables';
import { highlightLink } from '../../views/navigation/linkHighlight';

/**
 * Function loads settings page and updates content accordingly.
 */
export function loadSettingsPage() {
  clearContentBoxes();
  highlightLink('settings-link');
  loadContentBoxes('settings-boxes');
  updateArticle(
    'Nastavení uživatelských údajů',
    'Vyplň tabulku pro výpočet bazálního metabolismu a denního kalorického výdeje a zjisti tak svůj doporučený denní kalorický příjem.',
  );
  pageLogoImg.src = 'images/settings.png';
}
