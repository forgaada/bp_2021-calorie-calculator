import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { updateArticle } from '../../views/updateArticle';
import { pageLogoImg } from '../../utils/variables';
import { highlightLink } from '../../views/navigation/linkHighlight';

/**
 * Function sets article heading and content. Loads page with all activity boxes.
 */
export function loadActivityPage() {
  clearContentBoxes();
  highlightLink('activity-link');
  loadContentBoxes('activity-boxes');
  updateArticle(
    'Aktivity',
    'V této sekci můžeš vytvořit novou aktivitu. Aktivitu si poté můžeš najít a zapsat. Stejně tak i ostatní uživatelé.',
  );
  document.getElementById('activity-tags').innerHTML = '';
  document.getElementById('search-activity-content-all').innerHTML = 'Výsledky hledání: ';
  pageLogoImg.src = 'images/activities.png';
}
