import { loadLoginPage } from '../login/loginPageController';
import { hideDropdown } from '../../views/header/dropdown';
import { loadUserDataOnce } from '../database/user/loadUser';

/**
 * Function sets listener on login / logout from app.
 */
export function setAuthOnStatusChange() {
  auth.onAuthStateChanged((user) => {
    if (user) {
      userAuth = user;
      document.getElementById('nav-menu').classList.add('hide_auth');
      document.getElementById('nav-menu').classList.remove('show_auth');
      document.getElementById('user-content').classList.remove('before-login');
      document.getElementById('user-content').classList.add('after-login');

      // load user data
      loadUserDataOnce();
    } else {
      userAuth = null;
      document.getElementById('nav-menu').classList.add('show_auth');
      document.getElementById('nav-menu').classList.remove('hide_auth');
      document.getElementById('user-content').classList.add('before-login');
      document.getElementById('user-content').classList.remove('after-login');
      document.title = 'Přihlášení';
      loadLoginPage();
      hideDropdown();
    }
  });
}
