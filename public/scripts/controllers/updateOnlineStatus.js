/**
 * Function displays offline bar when there is no internet connection.
 * After successful reconnect offline bar updates.
 */
export function updateOnlineStatus() {
  let offlineBar = document.getElementById('offline-bar');
  if (navigator.onLine) {
    offlineBar.style.backgroundColor = 'green';
    offlineBar.innerText = 'Online';
    window.setTimeout(
      function () {
        offlineBar.style.display = 'none';
      }.bind(offlineBar),
      2000,
    );
  } else {
    offlineBar.innerText = 'Offline';
    offlineBar.style.backgroundColor = 'red';
    offlineBar.style.display = 'flex';
  }
}
