import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { updateArticle } from '../../views/updateArticle';
import { pageLogoImg } from '../../utils/variables';
import { highlightLink } from '../../views/navigation/linkHighlight';

/**
 * Function loads page with sign up content boxes.
 */
export function loadSignUpPage() {
  clearContentBoxes();
  highlightLink('signup-link');
  loadContentBoxes('signup-boxes');
  updateArticle('Registrace uživatele', 'Zaregistruj se do aplikace.');
  pageLogoImg.src = 'images/user_login.png';
}
