import { createUser } from '../database/user/createUser';

export let signUpForm = document.querySelector('#reg_form');

/**
 * Function adds 'onsubmit' event listener for adding new user to application.
 * If user's input is valid, user record is added to database using Firebase API.
 * After successful registration user is logged in to the app.
 */
export function addSignUpFormEventListener() {
  signUpForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // get user info
    const email = signUpForm['reg_mail'].value;
    const password = signUpForm['reg_password'].value;

    // sign up the user
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((cred) => {
        createUser(cred);
      })
      .catch((error) => {
        alert(error.message);
      });
  });
}
