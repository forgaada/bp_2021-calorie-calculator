import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { renderTodayMealsAndActivities } from '../../views/article/renderTodayMealsAndActivities';
import { updateArticle } from '../../views/updateArticle';
import { pageLogoImg } from '../../utils/variables';
import { highlightLink } from '../../views/navigation/linkHighlight';

/**
 * Function loads state page with user stats and updates content accordingly.
 */
export function loadStatePage() {
  clearContentBoxes();
  renderTodayMealsAndActivities(today);
  highlightLink('state-link');
  loadContentBoxes('state-boxes');
  updateArticle(
    'Denní přehled',
    'Zde nalezneš přehled svého denního kalorického příjmu a výdeje včetně zapsaných jídel a aktivit.',
  );
  pageLogoImg.src = 'images/prehled.png';
}
