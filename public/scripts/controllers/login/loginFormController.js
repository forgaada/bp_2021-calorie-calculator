export const logInForm = document.querySelector('#login_form');

/**
 * Function adds 'submit' event listener to login form.
 * When form is submitted, user logs in to app using Firebase API function.
 */
export function addLoginFormEventListener() {
  logInForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // get user info
    const email = logInForm['login_mail'].value;
    const password = logInForm['login_password'].value;

    auth
      .signInWithEmailAndPassword(email, password)
      .then((cred) => {
        console.log('User ' + cred.user.email + ' logged in!');
        // clear login form
        logInForm['login_mail'].value = '';
        logInForm['login_password'].value = '';
      })
      .catch((error) => {
        alert('Při přihlášení došlo k chybě: ' + error);
      });
  });
}
