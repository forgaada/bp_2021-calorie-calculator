// Loads login page with login form
import { clearContentBoxes, loadContentBoxes } from '../../views/article/contentBoxes';
import { updateArticle } from '../../views/updateArticle';
import { pageLogoImg } from '../../utils/variables';
import { highlightLink } from '../../views/navigation/linkHighlight';

/**
 * Function loads page with login content boxes.
 */
export function loadLoginPage() {
  clearContentBoxes();
  loadContentBoxes('login-boxes');
  highlightLink('login-link');
  updateArticle(
    'Přihlášení uživatele',
    `Přihlaš se. Pokud nemáš vytvořený účet, přejdi na <a href="#signup" style="text-decoration: none; color: #303030"><b>registraci</b></a>.`,
  );
  pageLogoImg.src = 'images/user_login.png';
}
