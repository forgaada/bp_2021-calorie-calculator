import { loadCreatedMealPage } from '../controllers/meals/createdMealPageController';

/**
 * Class containing all information about meal record in database.
 */
export class Meal {
  constructor(name, calories, protein, fat, sugars, ean, tags, amount, author_id) {
    this._name = name;
    this._calories = calories;
    this._protein = protein;
    this._fat = fat;
    this._sugars = sugars;
    this._ean = ean;
    this._tags = tags;
    this._amount = amount;
    this._author_id = author_id;
  }

  // Saves meal to Firestore
  async saveMeal() {
    try {
      let response = await db.collection('meals').add({
        name: this._name,
        calories: parseFloat(this._calories),
        protein: parseFloat(this._protein),
        fat: parseFloat(this._fat),
        sugars: parseFloat(this._sugars),
        ean: parseInt(this._ean),
        tags: this._tags,
        amount: this._amount,
        author_id: this._author_id,
      });
      console.log('Meal written with id = ', response.id);
      const createdMealId = response.id;
      loadCreatedMealPage(createdMealId);
    } catch (e) {
      console.error('Error adding meal: ', e);
    }
  }
}
