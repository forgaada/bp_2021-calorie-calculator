/**
   Class for rendering percentage donut charts via SVG path elements.
*/
export class SVGPieChart {
  constructor(svgElementId, size) {
    this._svg = document.querySelector(svgElementId);
    this._svgNs = 'http://www.w3.org/2000/svg';
    this._size = size;
  }

  /**
   * Function draws path element curved as a circle to create a donut chart
   * with full diameter.
   */
  drawBigChart(percentage, color, customChart, content) {
    let textContent = percentage;

    if (percentage === null) {
      percentage = 0;
    }
    if (percentage > 100) {
      percentage = 100;
    }
    let angle = percentage * 3.6;

    let pathElement = document.createElementNS(this._svgNs, 'path');
    pathElement.setAttributeNS(null, 'd', this.setArc(100, 100, angle));
    pathElement.setAttributeNS(null, 'stroke-width', '50');
    pathElement.setAttributeNS(null, 'stroke', color);
    pathElement.setAttributeNS(null, 'fill', 'none');

    let textElement = document.createElementNS(this._svgNs, 'text');
    textElement.setAttributeNS(null, 'x', '100');
    textElement.setAttributeNS(null, 'y', '100');
    textElement.setAttributeNS(null, 'fill', 'black');
    textElement.setAttributeNS(null, 'height', '40');
    textElement.setAttributeNS(null, 'text-anchor', 'middle');

    if (customChart) {
      textElement.textContent = textContent.toFixed(0) + ' % ' + content;
    }

    this._svg.appendChild(pathElement);
    this._svg.appendChild(textElement);
  }

  /**
   * Function draws smaller donut charge with half diameter.
   */
  drawSmallChart(percentage, color, customChart, content) {
    let textContent = percentage;

    if (percentage === null) {
      percentage = 0;
    }
    if (percentage > 100) {
      percentage = 100;
    }
    let angle = percentage * 3.6;

    let pathElement = document.createElementNS(this._svgNs, 'path');
    pathElement.setAttributeNS(null, 'd', this.setArc(50, 50, angle));
    pathElement.setAttributeNS(null, 'stroke-width', '20');
    pathElement.setAttributeNS(null, 'stroke', color);
    pathElement.setAttributeNS(null, 'fill', 'none');

    let textElement = document.createElementNS(this._svgNs, 'text');
    textElement.setAttributeNS(null, 'x', '50');
    textElement.setAttributeNS(null, 'y', '50');
    textElement.setAttributeNS(null, 'fill', 'black');
    textElement.setAttributeNS(null, 'height', '12');
    textElement.setAttributeNS(null, 'font-size', '11');
    textElement.setAttributeNS(null, 'text-anchor', 'middle');

    if (customChart) {
      textElement.textContent = textContent.toFixed(0) + '% ' + content;
    }

    this._svg.appendChild(pathElement);
    this._svg.appendChild(textElement);
  }

  /**
   * Function returns arc attributes for a path element.
   */
  setArc(x, y, angleTo) {
    let start = this.getCartesianCoordinates(x, y, angleTo);
    let end = this.getCartesianCoordinates(x, y, 0);

    let arcFlag = 0;
    if (angleTo >= 180) {
      arcFlag = 1;
    }

    return `M ${start.x} ${start.y} A ${x} ${y} 0 ${arcFlag} 0 ${end.x} ${end.y}`;
  }

  /**
   * Function returns coordinates to specific angle from center of coordinate system.
   */
  getCartesianCoordinates(centerX, centerY, angle) {
    let angleInRadians = ((angle - 90) * 3.14159) / 180.0;

    return {
      x: centerX + centerX * Math.cos(angleInRadians),
      y: centerY + centerY * Math.sin(angleInRadians),
    };
  }

  /**
   * Function resets SVG innerHTML content.
   */
  clear() {
    this._svg.innerHTML = '';
  }
}
