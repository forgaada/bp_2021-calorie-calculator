/**
 * Class for storing all the data about user.
 */
export class User {
  constructor(
    name,
    weight,
    height,
    year_of_birth,
    sex,
    activity,
    goal,
    eaten_meals,
    performed_activities,
  ) {
    this._name = name;
    this._weight = weight;
    this._height = height;
    this._year_of_birth = year_of_birth;
    this._sex = sex;
    this._activity = activity;
    this._goal = goal;
    this._basal = 0.01;
    this._recommended_income_protein = 0.01;
    this._recommended_income_fat = 0.01;
    this._recommended_income_sugars = 0.01;
    this._eaten_meals = eaten_meals;
    this._performed_activities = performed_activities;
    this._age = this.calculateAge(new Date(year_of_birth, 1, 1));
    this.calculateUsersOutcome(today);
  }

  /**
   * Function calculates user's age from date of birth and current date.
   * @param dob Date of birth
   * @returns {number} Age
   */
  calculateAge(dob) {
    let diff_ms = Date.now() - dob.getTime();
    let age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }

  /**
   * Function calculate user's total calories outcome for specific day based on
   * his basal metabolism and performed activities.
   * @param day Day specified in 'dd/mm/yyyy' format
   */
  calculateUsersOutcome(day) {
    let newBasal = 0.0;
    if (this._sex === 'female') {
      newBasal =
        655.0955 + 9.5634 * this._weight + 1.8496 * this._height - 4.6756 * this._age;
    } else if (this._sex === 'male') {
      newBasal =
        66.473 + 13.7516 * this._weight + 5.0033 * this._height - 6.755 * this._age;
    }
    this._basal = newBasal * this._activity * this._goal;

    for (let activity of this._performed_activities) {
      if (activity.date === day) {
        this._basal +=
          ((activity.met_index * 3.5 * this._weight) / 200) * activity.duration;
      }
    }

    this._recommended_income_protein = this._basal * 0.075;
    this._recommended_income_fat = this._basal * 0.033;
    this._recommended_income_sugars = this._basal * 0.1;
  }

  get basal() {
    return this._basal;
  }

  /**
   * Function saves user's data to database.
   */
  async saveUser() {
    try {
      const response = await db
        .collection('users')
        .doc(userAuth.uid)
        .set({
          name: this._name,
          weight: parseFloat(this._weight),
          height: parseFloat(this._height),
          year_of_birth: parseInt(this._year_of_birth),
          sex: this._sex,
          activity: parseFloat(this._activity),
          goal: parseFloat(this._goal),
          eaten_meals: this._eaten_meals,
          performed_activities: this._performed_activities,
        });
      console.log('User data successfully written!');
    } catch (error) {
      console.log('Error writing document: ', error);
    }
  }
}
