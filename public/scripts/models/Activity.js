/**
 * Class for describing data about an activity.
 */
export class Activity {
  constructor(name, met_index, tags, author_id) {
    this._name = name;
    this._met_index = met_index;
    this._tags = tags;
    this._author_id = author_id;
  }

  // Saves activity to Firestore
  async saveActivity() {
    try {
      let response = await db.collection('activities').add({
        name: this._name,
        met_index: parseInt(this._met_index),
        tags: this._tags,
        author_id: this._author_id,
      });
      console.log('Activity with id = ' + response.id + ' successfully written.');
    } catch (e) {
      console.log(e);
    }
  }
}
