export function displayDropdown() {
  document.getElementById('dropdown-div').style.display = 'inline-block';
}

export function hideDropdown() {
  document.getElementById('dropdown-div').style.display = 'none';
}
