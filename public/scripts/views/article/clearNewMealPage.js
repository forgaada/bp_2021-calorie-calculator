export function clearNewMealPage() {
  document.getElementById('meal-name-input').value = '';
  document.getElementById('meal-calories-input').value = '';
  document.getElementById('meal-protein-input').value = '';
  document.getElementById('meal-fat-input').value = '';
  document.getElementById('meal-sugars-input').value = '';
  document.getElementById('meal-ean-input').value = '';
  document.getElementById('new-meal-total-amount').value = '';
  document.querySelector('#added-tags').innerHTML = '';
}
