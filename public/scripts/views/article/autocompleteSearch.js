import { loadCreatedMealPage } from '../../controllers/meals/createdMealPageController';

/**
 * Function add listeners to input element and generates results of fulltext
 * searched meals based on user's latest input.
 *
 * @param inputId Input element id.
 * @param outputId Output div element id.
 */
export function autocomplete(inputId, outputId) {
  let searchInput = document.getElementById(inputId);
  let outputElement = document.getElementById(outputId);
  let records;

  searchInput.addEventListener('input', () => {
    records = [];
    outputElement.innerHTML = '';
    if (searchInput.value < 1) return;

    // create array of normalised strings from input value
    let normalisedInputString = searchInput.value
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .toLowerCase();
    let inputStrings = normalisedInputString.trim().split(' ');

    // convert all meals names to arrays of strings for better querying
    for (let doc of mealsSnapshot) {
      let rawString = doc.data().name;
      let normalisedString = rawString
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .toLowerCase();
      let arrayOfStrings = normalisedString.trim().split(' ');

      for (let subString of inputStrings) {
        if (
          arrayOfStrings.some(
            (keyword) => keyword.startsWith(subString) && keyword !== '-',
          )
        ) {
          records.push({ id: doc.id, data: doc.data() });
          break;
        }
      }
    }
    let resultNum = 0;
    for (let record of records) {
      resultNum++;
      if (resultNum === 6) break;

      let createdElement = document.createElement('a');
      createdElement.innerText = record.data.name;
      createdElement.style.cursor = 'pointer';
      createdElement.addEventListener('click', () => {
        loadCreatedMealPage(record.id);
      });

      outputElement.appendChild(createdElement);
    }
  });

  searchInput.addEventListener('focusout', () => {
    window.setTimeout(() => {
      searchInput.value = '';
      outputElement.innerHTML = '';
    }, 150);
  });
}
