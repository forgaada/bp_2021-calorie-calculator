import { loadStatePage } from '../../controllers/state/statePageController';
import { getUniqueID } from '../../controllers/utils/getId';
import { validateNumberInput } from '../../controllers/utils/validation/validateNumberInput';

/**
 * Function renders element in state page box content, containing description
 * of an activity performed by user in given day.
 *
 * @param activity Object containing information about activity record
 * @param day Date in 'dd/mm/yyyy' format
 */
export function renderActivity(activity, day) {
  if (activity.date !== day) {
    return;
  }
  const caloricOutcome =
    ((activity.met_index * 3.5 * _user._weight) / 200) * activity.duration;
  let createdElement = document.createElement('label');
  createdElement.classList.add('label-content');
  createdElement.classList.add('generated-content');
  createdElement.innerHTML = `<b style="font-size: 17px;">${activity.name}</b><br>
     <br>Výdej kalorií : ${caloricOutcome.toFixed(0)} kcal
     <br>Met index : ${activity.met_index}
     <br>Doba trvání: ${activity.duration} minut 
     <br>`;

  let createdDiv = document.createElement('div');
  createdDiv.classList.add('content_inline');

  let removeButton = document.createElement('button');
  createdDiv.appendChild(removeButton);
  removeButton.classList.add('content-button');
  removeButton.innerText = 'Smazat';
  removeButton.style.pointerEvents = 'auto';

  removeButton.addEventListener(
    'click',
    function () {
      _user._performed_activities = _user._performed_activities.filter(
        (element) => element.activity_record_id !== activity.activity_record_id,
      );
      _user.saveUser();
      loadStatePage();
    }.bind(activity),
  );

  // EDITING ACTIVITY
  let createdInputDiv = document.createElement('div');
  createdInputDiv.classList.add('content_inline_flex_start');
  createdInputDiv.style.display = 'none';
  createdElement.appendChild(createdInputDiv);

  let createdInputLabel = document.createElement('label');
  createdInputLabel.innerText = 'Doba trvání [min]';
  createdInputDiv.appendChild(createdInputLabel);

  let createdInput = document.createElement('input');
  createdInput.type = 'number';
  createdInput.id = getUniqueID();
  createdInputDiv.appendChild(createdInput);

  let confirmButton = document.createElement('button');
  confirmButton.classList.add('arrow-button');
  confirmButton.innerText = 'OK';
  createdInputDiv.appendChild(confirmButton);
  confirmButton.addEventListener(
    'click',
    function () {
      if (!validateNumberInput(createdInput.id)) return;
      let editedActivity = activity;
      editedActivity.duration = parseFloat(createdInput.value);

      _user._performed_activities = _user._performed_activities.filter(
        (element) => element.activity_record_id !== activity.activity_record_id,
      );
      _user._performed_activities.push(editedActivity);
      _user.saveUser();
      loadStatePage();
    }.bind(activity),
  );

  let editButton = document.createElement('button');
  editButton.classList.add('content-button');
  createdDiv.appendChild(editButton);
  editButton.innerText = 'Upravit';
  editButton.style.pointerEvents = 'auto';
  editButton.addEventListener('click', function () {
    if (createdInputDiv.style.display === 'none') {
      createdInputDiv.style.display = 'initial';
      editButton.innerText = 'Zrušit';
    } else {
      createdInputDiv.style.display = 'none';
      editButton.innerText = 'Upravit';
    }
  });

  createdElement.appendChild(createdDiv);

  document.querySelector('#today-activities').appendChild(createdElement);
}
