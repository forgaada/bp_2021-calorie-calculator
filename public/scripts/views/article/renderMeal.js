/*
    Renders meal's values into a new element but only for meal eaten today
*/
import { loadStatePage } from '../../controllers/state/statePageController';
import { getUniqueID } from '../../controllers/utils/getId';
import { validateNumberInput } from '../../controllers/utils/validation/validateNumberInput';

/**
 * Function renders element in state page box content, containing description
 * of a meal consumed by user in given day.
 *
 * @param meal Object containing information about meal record
 * @param day Date in 'dd/mm/yyyy' format
 */
export function renderMeal(meal, day) {
  if (meal.date !== day) {
    return;
  }

  // load meal data
  for (let snapshot of mealsSnapshot) {
    if (snapshot.id === meal.meal_id) {
      let mealSnapshot = snapshot.data();
      let amount_type = mealSnapshot.amount;

      let createdElement = document.createElement('label');
      createdElement.classList.add('label-content');
      createdElement.classList.add('generated-content');
      createdElement.innerHTML = `<b style="font-size: 17px;">${mealSnapshot.name}</b><br>
          <br>S hodnotami na ${amount_type}:
          <br>Kalorie : ${mealSnapshot.calories} kcal
          <br>Bílkoviny : ${mealSnapshot.protein} g
          <br>Tuky : ${mealSnapshot.fat} g
          <br>Sacharidy : ${mealSnapshot.sugars} g
          <br><br>V množství: ${meal.amount} g
          <br>`;

      if (amount_type === '100g' || amount_type === '100ml') {
        amount_type = 100;
      } else {
        amount_type = 1;
      }

      switch (meal.meal_type) {
        case 'Snídaně': {
          document.querySelector('#today-meals-1').appendChild(createdElement);
          break;
        }
        case 'Svačina': {
          document.querySelector('#today-meals-2').appendChild(createdElement);
          break;
        }
        case 'Oběd': {
          document.querySelector('#today-meals-3').appendChild(createdElement);
          break;
        }
        case 'Odpolední svačina': {
          document.querySelector('#today-meals-4').appendChild(createdElement);
          break;
        }
        case 'Večeře': {
          document.querySelector('#today-meals-5').appendChild(createdElement);
          break;
        }
        default: {
          document.querySelector('#today-meals-3').appendChild(createdElement);
          break;
        }
      }

      // update daily income variables
      caloriesIncome += (mealSnapshot.calories * meal.amount) / amount_type;
      proteinIncome += (mealSnapshot.protein * meal.amount) / amount_type;
      fatIncome += (mealSnapshot.fat * meal.amount) / amount_type;
      sugarsIncome += (mealSnapshot.sugars * meal.amount) / amount_type;

      let createdDiv = document.createElement('div');
      createdDiv.classList.add('content_inline');

      let createdButton = document.createElement('button');
      createdDiv.appendChild(createdButton);
      createdButton.classList.add('content-button');
      createdButton.innerText = 'Smazat';
      // Adds eventListener for delete button "click" event.
      // Removes meal element and subtracts nutrition values from stats.
      createdButton.addEventListener(
        'click',
        function () {
          _user._eaten_meals = _user._eaten_meals.filter(
            (element) => element.meal_record_id !== meal.meal_record_id,
          );
          _user.saveUser();
          loadStatePage();
        }.bind(meal),
      );

      // EDITING MEAL
      let createdInputDiv = document.createElement('div');
      createdInputDiv.classList.add('content_inline_flex_start');
      createdInputDiv.style.display = 'none';
      createdElement.appendChild(createdInputDiv);

      let createdInputLabel = document.createElement('label');
      createdInputLabel.innerText = 'Množství: ';
      createdInputDiv.appendChild(createdInputLabel);

      let createdInput = document.createElement('input');
      createdInput.type = 'number';
      createdInput.id = getUniqueID();
      createdInputDiv.appendChild(createdInput);

      let confirmButton = document.createElement('button');
      confirmButton.classList.add('arrow-button');
      confirmButton.innerText = 'OK';
      createdInputDiv.appendChild(confirmButton);
      confirmButton.addEventListener(
        'click',
        function () {
          if (!validateNumberInput(createdInput.id)) return;
          _user._eaten_meals.forEach((element) => {
            if (element.meal_record_id === meal.meal_record_id) {
              element.amount = parseFloat(createdInput.value);
            }
          });
          _user.saveUser();
          loadStatePage();
        }.bind(meal),
      );

      let editButton = document.createElement('button');
      editButton.classList.add('content-button');
      createdDiv.appendChild(editButton);
      editButton.innerText = 'Upravit';
      editButton.style.pointerEvents = 'auto';
      editButton.addEventListener('click', function () {
        if (createdInputDiv.style.display === 'none') {
          createdInputDiv.style.display = 'initial';
          editButton.innerText = 'Zrušit';
        } else {
          createdInputDiv.style.display = 'none';
          editButton.innerText = 'Upravit';
        }
      });

      createdElement.appendChild(createdDiv);
      break;
    }
  }
}
