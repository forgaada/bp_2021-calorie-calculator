export function displayTodayMeals() {
  document.getElementById('today-meals-0').style.display = 'flex';
  document.getElementById('today-meals-1').style.display = 'flex';
  document.getElementById('today-meals-2').style.display = 'flex';
  document.getElementById('today-meals-3').style.display = 'flex';
  document.getElementById('today-meals-4').style.display = 'flex';
  document.getElementById('today-meals-5').style.display = 'flex';
}

export function hideTodayMeals() {
  document.getElementById('today-meals-0').style.display = 'none';
  document.getElementById('today-meals-1').style.display = 'none';
  document.getElementById('today-meals-2').style.display = 'none';
  document.getElementById('today-meals-3').style.display = 'none';
  document.getElementById('today-meals-4').style.display = 'none';
  document.getElementById('today-meals-5').style.display = 'none';
}
