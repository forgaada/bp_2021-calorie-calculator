/**
 * This function adds listeners on checkboxes for hiding / displaying passwords.
 */
export function addPasswordsCheckboxesListeners() {
  const regPasswordCheckbox = document.getElementById('reg-password-checkbox');
  const regPasswordInput = document.getElementById('reg_password');
  regPasswordCheckbox.addEventListener('change', function () {
    if (this.checked) {
      regPasswordInput.type = 'text';
    } else {
      regPasswordInput.type = 'password';
    }
  });
  const loginPasswordCheckbox = document.getElementById('login-password-checkbox');
  const loginPasswordInput = document.getElementById('login_password');
  loginPasswordCheckbox.addEventListener('change', function () {
    if (this.checked) {
      loginPasswordInput.type = 'text';
    } else {
      loginPasswordInput.type = 'password';
    }
  });
}
