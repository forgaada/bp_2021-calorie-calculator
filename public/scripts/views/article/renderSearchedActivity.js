import { getUniqueID } from '../../controllers/utils/getId';
import { deleteActivityById } from '../../controllers/database/activities/deleteActivity';
import { loadActivityPage } from '../../controllers/activities/activityPageController';
import { ADMIN_EMAIL } from '../../utils/variables';

/**
 * Function renders label element containing searched activity result description.
 * @param activityId
 * @param activityRecord
 */
export function renderSearchedActivity(activityId, activityRecord) {
  let createdElement = document.createElement('label');
  const caloricOutcome = ((activityRecord.met_index * 3.5 * _user._weight) / 200) * 60;
  createdElement.classList.add('label-content');
  createdElement.classList.add('generated-content');
  createdElement.innerHTML = `<b style="font-size: 17px;">${activityRecord.name}</b><br>
     <br>Výdej kalorií : ${caloricOutcome.toFixed(0)} kcal / hod
     <br>Met index : ${activityRecord.met_index}<br>`;

  let createdInputDiv = document.createElement('div');
  createdInputDiv.classList.add('content_inline');
  createdInputDiv.style.display = 'none';
  createdElement.appendChild(createdInputDiv);

  let createdDiv = document.createElement('div');
  createdDiv.classList.add('content_inline');
  createdElement.appendChild(createdDiv);

  let createdLabel = document.createElement('label');
  createdLabel.innerText = 'Doba trvání [min]: ';
  createdLabel.style.height = '40px';
  createdInputDiv.appendChild(createdLabel);

  let createdInput = document.createElement('input');
  createdInput.type = 'number';
  createdInput.id = activityRecord.name + '-input';
  createdInputDiv.appendChild(createdInput);

  let confirmButton = document.createElement('button');
  confirmButton.classList.add('arrow-button');
  confirmButton.innerText = 'OK';
  confirmButton.id = activityRecord.name + '-confirm-button';
  createdInputDiv.appendChild(confirmButton);
  confirmButton.addEventListener('click', () => {
    if (createdInput.value === undefined || createdInput.value <= 0) {
      return;
    }
    let record = {
      name: activityRecord.name,
      duration: parseFloat(createdInput.value),
      met_index: activityRecord.met_index,
      activity_record_id: getUniqueID(),
      date: today,
    };
    _user._performed_activities.push(record);
    _user.saveUser();
    window.location.hash = '#state';
  });

  let createdAddButton = document.createElement('button');
  createdAddButton.classList.add('content-button');
  createdAddButton.innerText = 'Zapsat';
  createdAddButton.id = activityRecord.name + '-add-button';
  createdDiv.appendChild(createdAddButton);

  createdAddButton.addEventListener('click', function (e) {
    if (createdInputDiv.style.display === 'none') {
      createdInputDiv.style.display = 'initial';
      createdAddButton.innerText = 'Zrušit';
    } else {
      createdInputDiv.style.display = 'none';
      createdAddButton.innerText = 'Zapsat';
    }
  });

  if (activityRecord.author_id === userAuth.uid || userAuth.email === ADMIN_EMAIL) {
    let createdDeleteButton = document.createElement('button');
    createdDeleteButton.classList.add('content-button');
    createdDeleteButton.innerText = 'Smazat';
    createdDiv.appendChild(createdDeleteButton);

    createdDeleteButton.addEventListener('click', function (e) {
      deleteActivityById(activityId);
      loadActivityPage();
    });
  }

  document.getElementById('search-activity-content-all').appendChild(createdElement);
}
