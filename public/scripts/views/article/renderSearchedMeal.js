import { loadCreatedMealPage } from '../../controllers/meals/createdMealPageController';
import { getUniqueID } from '../../controllers/utils/getId';
import { validateNumberInput } from '../../controllers/utils/validation/validateNumberInput';
import { addToNewMeal } from '../../controllers/meals/addToNewMeal';

/**
 * Function renders label element containing searched meal result description.
 * @param mealId
 * @param mealRecord
 */
export function renderSearchedMeal(mealId, mealRecord) {
  let createdElement = document.createElement('label');
  createdElement.classList.add('label-content');
  createdElement.classList.add('generated-content');
  createdElement.innerHTML = `<b style="font-size: 17px;">${mealRecord.name}</b><br>
          <br>S hodnotami na ${mealRecord.amount}:
          <br>Kalorie : ${mealRecord.calories} kcal
          <br>Bílkoviny : ${mealRecord.protein} g
          <br>Tuky : ${mealRecord.fat} g
          <br>Sacharidy : ${mealRecord.sugars} g
          <br>`;

  // creating input element for adding values to new meal
  let createdInputDiv = document.createElement('div');
  createdInputDiv.classList.add('content_inline_flex_start');
  createdInputDiv.style.display = 'none';
  createdElement.appendChild(createdInputDiv);

  let createdInputLabel = document.createElement('label');
  createdInputLabel.innerText = 'Množství [g]: ';
  createdInputDiv.appendChild(createdInputLabel);

  let createdInput = document.createElement('input');
  createdInput.type = 'number';
  createdInput.id = getUniqueID();
  createdInputDiv.appendChild(createdInput);

  let confirmButton = document.createElement('button');
  confirmButton.classList.add('arrow-button');
  confirmButton.innerText = 'OK';
  createdInputDiv.appendChild(confirmButton);
  confirmButton.addEventListener('click', () => {
    if (!validateNumberInput(createdInput.id)) return;
    addToNewMeal(mealRecord, createdInput.value);
    window.location.hash = '#newmeal';
  });

  // adding show meal / add meal buttons
  let createdDiv = document.createElement('div');
  createdDiv.classList.add('content_inline');
  createdElement.appendChild(createdDiv);

  let showMealButton = document.createElement('button');
  showMealButton.classList.add('content-button');
  showMealButton.innerText = 'Zapsat';
  createdDiv.appendChild(showMealButton);

  let addToNewMealButton = document.createElement('button');
  addToNewMealButton.classList.add('content-button');
  addToNewMealButton.innerText = 'Přidat k novému';
  createdDiv.appendChild(addToNewMealButton);

  // adding event listeners to buttons
  showMealButton.addEventListener('click', function (e) {
    e.preventDefault();
    loadCreatedMealPage(mealId);
  });

  addToNewMealButton.addEventListener('click', function (e) {
    if (createdInputDiv.style.display === 'none') {
      createdInputDiv.style.display = 'initial';
      addToNewMealButton.innerText = 'Zrušit';
    } else {
      createdInputDiv.style.display = 'none';
      addToNewMealButton.innerText = 'Přidat k novému';
    }
  });

  document.getElementById('search-meal-content-all').appendChild(createdElement);
}
