// Renders all meals eaten on the specific day and creates buttons for browsing also the previous days meals.
import { loadPieChartData } from '../loadPieChartData';
import { renderActivity } from './renderActivity';
import { renderMeal } from './renderMeal';
import { todayDate } from '../../utils/variables';
import { displayTodayMeals, hideTodayMeals } from './displayTodayMeals';

/**
 * Function fully updates state page data including rendering all performed
 * activities and consumed meals for current day.
 *
 * @param day Day in 'dd/mm/yyyy' date format
 */
export function renderTodayMealsAndActivities(day) {
  document.getElementById('date-control').innerHTML = ``;

  // Rendering activities
  document.getElementById('activities-checkbox').innerHTML = ``;
  document.getElementById('activities-checkbox').addEventListener('change', function () {
    if (this.checked) {
      document.getElementById('today-activities').style.display = 'flex';
    } else {
      document.getElementById('today-activities').style.display = 'none';
    }
  });
  document.getElementById('today-activities').innerHTML = '';

  // add new activity button
  let activitiesDiv = document.createElement('div');
  activitiesDiv.classList.add('content_inline_center');

  let activitiesLabel = document.createElement('label');
  activitiesLabel.innerText = 'Přidat aktivitu';
  activitiesLabel.style.display = 'flex';
  activitiesLabel.style.justifyContent = 'center';
  activitiesLabel.style.alignItems = 'center';

  let addActivityButton = document.createElement('button');
  addActivityButton.classList.add('arrow-button');
  addActivityButton.innerText = '+';
  addActivityButton.addEventListener('click', () => {
    window.location.hash = '#activity';
    document
      .getElementById('search-activity-input')
      .scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    document.getElementById('search-activity-input').focus({ preventScroll: false });
  });

  activitiesDiv.appendChild(activitiesLabel);
  activitiesDiv.appendChild(addActivityButton);
  document.getElementById('today-activities').appendChild(activitiesDiv);

  // Rendering meals
  document.getElementById('meals-checkbox').innerHTML = ``;
  document.getElementById('meals-checkbox').addEventListener('change', function () {
    if (this.checked) {
      displayTodayMeals();
    } else {
      hideTodayMeals();
    }
  });

  // add new meal button
  let mealsDiv = document.createElement('div');
  mealsDiv.classList.add('content_inline_center');

  let mealsLabel = document.createElement('label');
  mealsLabel.innerText = 'Přidat jídlo';
  mealsLabel.style.display = 'flex';
  mealsLabel.style.justifyContent = 'center';
  mealsLabel.style.alignItems = 'center';

  let addMealButton = document.createElement('button');
  addMealButton.classList.add('arrow-button');
  addMealButton.innerText = '+';
  addMealButton.addEventListener('click', () => {
    window.location.hash = '#meals';
    document
      .getElementById('search-meal-input')
      .scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    document.getElementById('search-meal-input').focus({ preventScroll: false });
  });

  mealsDiv.appendChild(mealsLabel);
  mealsDiv.appendChild(addMealButton);

  document.getElementById('today-meals-0').innerHTML = '';
  document.getElementById('today-meals-0').appendChild(mealsDiv);
  document.getElementById('today-meals-1').innerHTML = 'Snídaně: ';
  document.getElementById('today-meals-2').innerHTML = 'Svačina: ';
  document.getElementById('today-meals-3').innerHTML = 'Oběd: ';
  document.getElementById('today-meals-4').innerHTML = 'Odpolední svačina: ';
  document.getElementById('today-meals-5').innerHTML = 'Večeře: ';

  let dateControl = document.getElementById('date-control');

  let leftArrowImage = new Image();
  leftArrowImage.src = 'images/left.png';
  leftArrowImage.alt = 'left-arrow-image';
  leftArrowImage.style.marginTop = '4px';
  leftArrowImage.style.marginRight = '2px';

  let createdButtonLastDay = document.createElement('button');
  createdButtonLastDay.classList.add('arrow-button');
  createdButtonLastDay.name = 'last-day';
  createdButtonLastDay.style.borderRight = '40px blue';
  createdButtonLastDay.appendChild(leftArrowImage);
  createdButtonLastDay.addEventListener('click', () => {
    let d = todayDate;
    d.setDate(d.getDate() - 1);
    let dd = String(d.getDate()).padStart(2, '0');
    let mm = String(d.getMonth() + 1).padStart(2, '0');
    let yyyy = d.getFullYear();
    d = dd + '/' + mm + '/' + yyyy;
    renderTodayMealsAndActivities(d);
  });

  let dateLogo = new Image();
  dateLogo.src = 'images/date-logo.png';
  dateLogo.alt = 'date-logo';
  dateLogo.height = 20;
  dateLogo.width = 20;
  dateLogo.style.marginTop = 'auto';
  dateLogo.style.marginBottom = 'auto';

  let createdLabel = document.createElement('label');
  const parsedDay = day.replace(/\//g, '.');
  createdLabel.style.display = 'flex';
  createdLabel.innerText = parsedDay;
  createdLabel.style.alignItems = 'center';
  createdLabel.style.color = 'grey';

  let rightArrowImage = new Image();
  rightArrowImage.src = 'images/right.png';
  rightArrowImage.alt = 'right-arrow-image';
  rightArrowImage.style.marginTop = '4px';
  rightArrowImage.style.marginLeft = '2px';

  let createdButtonNextDay = document.createElement('button');
  createdButtonNextDay.classList.add('arrow-button');
  createdButtonNextDay.name = 'next-day';
  createdButtonNextDay.appendChild(rightArrowImage);
  createdButtonNextDay.addEventListener('click', () => {
    let d = todayDate;
    d.setDate(d.getDate() + 1);
    let dd = String(d.getDate()).padStart(2, '0');
    let mm = String(d.getMonth() + 1).padStart(2, '0');
    let yyyy = d.getFullYear();
    d = dd + '/' + mm + '/' + yyyy;
    renderTodayMealsAndActivities(d);
  });

  dateControl.appendChild(createdButtonLastDay);
  dateControl.appendChild(dateLogo);
  dateControl.appendChild(createdLabel);
  dateControl.appendChild(createdButtonNextDay);

  caloriesIncome = 0;
  proteinIncome = 0;
  fatIncome = 0;
  sugarsIncome = 0;

  // Render all activites by specific day.
  for (let activity of _user._performed_activities) {
    renderActivity(activity, day);
  }

  // Render all meals by specific day.
  for (let meal of _user._eaten_meals) {
    renderMeal(meal, day);
  }

  loadPieChartData(day);
}
