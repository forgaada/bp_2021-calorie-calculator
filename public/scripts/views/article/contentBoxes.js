/**
 * Function hides all elements with '.content-box' class from page.
 */
export function clearContentBoxes() {
  for (let element of document.getElementsByClassName('content-box')) {
    element.style.display = 'none';
  }
}

/**
 * Function displays all elements with given className on page.
 * @param className
 */
export function loadContentBoxes(className) {
  for (let element of document.getElementsByClassName(className)) {
    element.style.display = 'flex';
  }
}
