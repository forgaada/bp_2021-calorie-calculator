export function updateArticle(heading, content) {
  document.getElementById('article-heading').innerHTML = heading;
  document.getElementById('article-content').innerHTML = content;
}
