/*
   Loads state page elements. Dumps user's income and basal values and all today's meals.
   Based on income / outcome (basal) ratio creates SVGPieChart of today's income percentage.
*/
import { SVGPieChart } from '../models/SVGPieChart';

/**
 * Function renders donut chart data describing today's income using SVGPieChart object.
 * @param day Date in 'dd/mm/yyyy' format
 */
export function loadPieChartData(day) {
  // Recalculate users outcome
  _user.calculateUsersOutcome(day);

  // CALORIES CHART
  document.getElementById('pie-chart').style.display = 'initial';
  if (pieChart === null) {
    pieChart = new SVGPieChart('#pie-chart');
  } else {
    pieChart.clear();
  }

  let percentage_for_calories = (caloriesIncome / _user.basal) * 100; // pocet kalorii ku basalu

  pieChart.drawBigChart(100, 'grey', false);
  if (percentage_for_calories < 50) {
    pieChart.drawBigChart(percentage_for_calories, 'lime', true, 'kcal');
  } else if (percentage_for_calories < 75) {
    pieChart.drawBigChart(percentage_for_calories, 'orange', true, 'kcal');
  } else {
    pieChart.drawBigChart(percentage_for_calories, 'red', true, 'kcal');
  }

  // USER-BOX CHART
  document.getElementById('pie-chart-user-box').style.display = 'initial';
  if (pieChartUserBox === null) {
    pieChartUserBox = new SVGPieChart('#pie-chart-user-box');
  } else {
    pieChartUserBox.clear();
  }
  pieChartUserBox.drawSmallChart(100, 'grey', false);
  if (percentage_for_calories < 50) {
    pieChartUserBox.drawSmallChart(percentage_for_calories, 'lime', true, '[kcal]');
  } else if (percentage_for_calories < 75) {
    pieChartUserBox.drawSmallChart(percentage_for_calories, 'orange', true, '[kcal]');
  } else {
    pieChartUserBox.drawSmallChart(percentage_for_calories, 'red', true, '[kcal]');
  }

  // PROTEIN CHART
  let percentage_for_protein = (proteinIncome / _user._recommended_income_protein) * 100;

  document.getElementById('pie-chart-protein').style.display = 'initial';
  if (pieChartProtein === null) {
    pieChartProtein = new SVGPieChart('#pie-chart-protein');
  } else {
    pieChartProtein.clear();
  }
  pieChartProtein.drawSmallChart(100, 'grey', false);
  if (percentage_for_protein < 50) {
    pieChartProtein.drawSmallChart(percentage_for_protein, 'lime', true, 'bílkovin');
  } else if (percentage_for_protein < 75) {
    pieChartProtein.drawSmallChart(percentage_for_protein, 'orange', true, 'bílkovin');
  } else {
    pieChartProtein.drawSmallChart(percentage_for_protein, 'red', true, 'bílkovin');
  }

  // FAT CHART
  let percentage_for_fat = (fatIncome / _user._recommended_income_fat) * 100;

  document.getElementById('pie-chart-protein').style.display = 'initial';
  if (pieChartFat === null) {
    pieChartFat = new SVGPieChart('#pie-chart-fat');
  } else {
    pieChartFat.clear();
  }
  pieChartFat.drawSmallChart(100, 'grey', false);
  if (percentage_for_fat < 50) {
    pieChartFat.drawSmallChart(percentage_for_fat, 'lime', true, 'tuků');
  } else if (percentage_for_fat < 75) {
    pieChartFat.drawSmallChart(percentage_for_fat, 'orange', true, 'tuků');
  } else {
    pieChartFat.drawSmallChart(percentage_for_fat, 'red', true, 'tuků');
  }

  // SUGARS CHART
  let percentage_for_sugars = (sugarsIncome / _user._recommended_income_sugars) * 100;

  document.getElementById('pie-chart-protein').style.display = 'initial';
  if (pieChartSugars === null) {
    pieChartSugars = new SVGPieChart('#pie-chart-sugars');
  } else {
    pieChartSugars.clear();
  }
  pieChartSugars.drawSmallChart(100, 'grey', false);
  if (percentage_for_sugars < 50) {
    pieChartSugars.drawSmallChart(percentage_for_sugars, 'lime', true, 'sacharidů');
  } else if (percentage_for_sugars < 75) {
    pieChartSugars.drawSmallChart(percentage_for_sugars, 'orange', true, 'sacharidů');
  } else {
    pieChartSugars.drawSmallChart(percentage_for_sugars, 'red', true, 'sacharidů');
  }

  document.getElementById('calories-income-label').innerHTML =
    'Kalorie <br><b>' +
    caloriesIncome.toFixed(0) +
    ' kcal </b> z ' +
    _user.basal.toFixed(0) +
    ' kcal';
  document.getElementById('protein-income-label').innerHTML =
    'Bílkoviny <br><b>' +
    proteinIncome.toFixed(0) +
    ' g </b> z ' +
    _user._recommended_income_protein.toFixed(0) +
    ' g <br>';
  document.getElementById('fats-income-label').innerHTML =
    'Tuky <br><b>' +
    fatIncome.toFixed(0) +
    ' g </b> z ' +
    _user._recommended_income_fat.toFixed(0) +
    ' g <br>';
  document.getElementById('sugars-income-label').innerHTML =
    'Sacharidy <br><b>' +
    sugarsIncome.toFixed(0) +
    ' g </b> z ' +
    _user._recommended_income_sugars.toFixed(0) +
    ' g <br>';
}
