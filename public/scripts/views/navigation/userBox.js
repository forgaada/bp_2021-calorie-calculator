// update user content box
import { getRandomColor } from '../../controllers/utils/colorGenerator';

export function updateUserBox() {
  if (userAuth) {
    // create SVG with username
    let svg = document.getElementById('svg-username');
    let svgNs = 'http://www.w3.org/2000/svg';
    svg.innerHTML = ``;

    // create circle element
    let circleElement = document.createElementNS(svgNs, 'circle');
    circleElement.setAttributeNS(null, 'cx', '50');
    circleElement.setAttributeNS(null, 'cy', '50');
    circleElement.setAttributeNS(null, 'r', '50');
    circleElement.setAttributeNS(null, 'fill', getRandomColor()); //'#aeaeae' or getRandomColor()

    // create text element
    let textElement = document.createElementNS(svgNs, 'text');
    textElement.setAttributeNS(null, 'x', '50%');
    textElement.setAttributeNS(null, 'y', '50%');
    textElement.setAttributeNS(null, 'fill', 'white');
    textElement.setAttributeNS(null, 'text-anchor', 'middle');
    textElement.setAttributeNS(null, 'font-size', '50px');
    textElement.setAttributeNS(null, 'font-family', 'Arial');
    textElement.setAttributeNS(null, 'dy', '.35em');
    textElement.textContent = _user._name[0];

    // append elements to svg object
    svg.appendChild(circleElement);
    svg.appendChild(textElement);

    document.getElementById('user-name').innerText =
      ' Uživatel: ' +
      _user._name +
      '\r' +
      ' Rok narození: ' +
      _user._year_of_birth +
      '\r' +
      ' Hmotnost: ' +
      _user._weight +
      ' kg' +
      '\r \r' +
      ' Denní příjem: ';
  }
}
