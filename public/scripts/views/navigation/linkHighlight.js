export function resetLinksHighlights() {
  let elements = [];
  elements.push(document.getElementById('login-link'));
  elements.push(document.getElementById('logout-link'));
  elements.push(document.getElementById('activity-link'));
  elements.push(document.getElementById('meals-link'));
  elements.push(document.getElementById('newmeal-link'));
  elements.push(document.getElementById('settings-link'));
  elements.push(document.getElementById('state-link'));
  elements.push(document.getElementById('signup-link'));

  for (let element of elements) {
    element.classList.add('unselected');
    element.classList.remove('selected');
  }
}

export function highlightLink(linkId) {
  document.getElementById(linkId).classList.remove('unselected');
  document.getElementById(linkId).classList.add('selected');
}
