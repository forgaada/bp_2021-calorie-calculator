export let meals = [];

// Initialize today's Date
export let todayDate = new Date();
// Initialize images
export let pageLogoImg = document.getElementById('page-logo');
// Initialize constants
export const RESULTS_NUM = 3;
export const ADMIN_EMAIL = 'admin@email.cz';
