import QrScanner from './qr-scanner.min';
import { loadCreatedMealPage } from '../controllers/meals/createdMealPageController';
import { scanning_start_button } from '../controllers/buttons/scanningStartButton';

export let qrScanner = null;
export let isScanning = false;

/**
 * Function creates new QrScanner object and tries to access camera for scanning
 * and triggers QrScanner start() function.
 */
export function startScanning() {
  isScanning = true;
  QrScanner.WORKER_PATH = 'scripts/qr_code_scanner/qr-scanner-worker.min.js';
  qrScanner = new QrScanner(document.getElementById('qr-video'), (result) => {
    let parsedResult = result.substring(46);
    if (parsedResult.length > 0) {
      qrScanner.stop();
      loadCreatedMealPage(parsedResult);
    } else {
      stopScanning();
      alert('Jídlo s uvedeným QR kódem nelze nalézt!');
      scanning_start_button.innerText = 'Start';
      scanning_start_button.style.backgroundColor = '#2fd015';
    }
  });
  qrScanner.start();
}

/**
 * Function triggers QrScanner stop() function.
 */
export function stopScanning() {
  isScanning = false;
  qrScanner.stop();
}
