/**
 * Function validates number input on being non-negative, non-empty and number values.
 * Eventually adds error class to invalidated inputs.
 * @param inputElementId Id HTML tag of input element.
 * @returns {boolean} Validation result (true - passed, false - not passed)
 */
function validateNumberInput(inputElementId) {
  let element = document.getElementById(inputElementId);
  if (element.value < 0 || element.value === '' || isNaN(element.value)) {
    element.classList.add('error');
    return false;
  } else if (element.classList.contains('error')) {
    element.classList.remove('error');
    return true;
  }
  return true;
}

/**
 * Validates text input on being non-empty and up to 50 chars long.
 * Eventually input turns red.
 * @param inputElementId Id HTML tag of input element.
 * @returns {boolean} Validation result (true - passed, false - not passed)
 */
function validateTextInput(inputElementId) {
  let element = document.getElementById(inputElementId);
  if (element.value === '' || element.value.length > 50) {
    element.classList.add('error');
    return false;
  } else if (element.classList.contains('error')) {
    element.classList.remove('error');
    return true;
  }
  return true;
}

/**
 * Function runs validateNumberInput and validateTextInput functions on both
 * number and text inputs.
 *
 * @param numberInputs Array of number input elements ids.
 * @param textInputs Array of text input elements ids.
 * @returns {boolean} Boolean containing information whether both validations
 * passed successfully or not.
 */
function validate(numberInputs, textInputs) {
  let success = true;
  for (let id of numberInputs) {
    if (!validateNumberInput(id)) {
      success = false;
    }
  }
  for (let tid of textInputs) {
    if (!validateTextInput(tid)) {
      success = false;
    }
  }
  return success;
}

/**
 * Class for describing data about an activity.
 */
class Activity {
  constructor(name, met_index, tags, author_id) {
    this._name = name;
    this._met_index = met_index;
    this._tags = tags;
    this._author_id = author_id;
  }

  // Saves activity to Firestore
  async saveActivity() {
    try {
      let response = await db.collection('activities').add({
        name: this._name,
        met_index: parseInt(this._met_index),
        tags: this._tags,
        author_id: this._author_id,
      });
      console.log('Activity with id = ' + response.id + ' successfully written.');
    } catch (e) {
      console.log(e);
    }
  }
}

const activity_button = document.getElementById('activity_button');

/**
 * Function adds listener on button for adding new activity record to Firestore.
 */
function addActivityButtonEventListener() {
  activity_button.addEventListener('click', function (e) {
    e.preventDefault();

    let textElementsToValidate = ['activity-name-input'];
    let numberElementsToValidate = ['activity-met-index-input'];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }
    let tags = [];
    for (let element of document.querySelector('#activity-tags').childNodes) {
      tags.push(element.textContent);
    }
    let newActivity = new Activity(
      document.getElementById('activity-name-input').value,
      document.getElementById('activity-met-index-input').value,
      tags,
      userAuth.uid,
    );
    newActivity.saveActivity();
    document.getElementById('activity-name-input').value = '';
    document.getElementById('activity-met-index-input').value = '';

    document.querySelector('#activity-tags').innerHTML = ``;
  });
}

const logInForm = document.querySelector('#login_form');

/**
 * Function adds 'submit' event listener to login form.
 * When form is submitted, user logs in to app using Firebase API function.
 */
function addLoginFormEventListener() {
  logInForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // get user info
    const email = logInForm['login_mail'].value;
    const password = logInForm['login_password'].value;

    auth
      .signInWithEmailAndPassword(email, password)
      .then((cred) => {
        console.log('User ' + cred.user.email + ' logged in!');
        // clear login form
        logInForm['login_mail'].value = '';
        logInForm['login_password'].value = '';
      })
      .catch((error) => {
        alert('Při přihlášení došlo k chybě: ' + error);
      });
  });
}

async function createUser(cred) {
  try {
    const response = await db.collection('users').doc(cred.user.uid).set({
      name: signUpForm['reg_name'].value,
      weight: 80.0,
      height: 175.0,
      year_of_birth: 1990.0,
      sex: 'male',
      activity: 1.0,
      goal: 1.0,
      eaten_meals: [],
      performed_activities: [],
    });

    // clear sign up form data
    signUpForm['reg_mail'].value = '';
    signUpForm['reg_password'].value = '';
    signUpForm['reg_name'].value = '';

    console.log('User successfully registered!');
  } catch (error) {
    console.log('Error occurred during creating new user: ' + error);
  }
}

let signUpForm = document.querySelector('#reg_form');

/**
 * Function adds 'onsubmit' event listener for adding new user to application.
 * If user's input is valid, user record is added to database using Firebase API.
 * After successful registration user is logged in to the app.
 */
function addSignUpFormEventListener() {
  signUpForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // get user info
    const email = signUpForm['reg_mail'].value;
    const password = signUpForm['reg_password'].value;

    // sign up the user
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((cred) => {
        createUser(cred);
      })
      .catch((error) => {
        alert(error.message);
      });
  });
}

const add_activity_tag_button = document.getElementById('add-activity-tag-button');

/**
 * Function adds listener on button for adding new tag to created activity.
 */
function addNewActivityTagButtonEventListener() {
  add_activity_tag_button.addEventListener('click', function (e) {
    let textElementsToValidate = ['add-activity-tag'];
    if (!validate([], textElementsToValidate)) {
      return;
    }

    let createdElement = document.createElement('label');
    createdElement.classList.add('tag');
    createdElement.innerText = document.getElementById('add-activity-tag').value;
    document.getElementById('add-activity-tag').value = '';
    document.querySelector('#activity-tags').appendChild(createdElement);

    createdElement.addEventListener('click', () => {
      document.querySelector('#activity-tags').removeChild(createdElement);
    });
  });
}

/**
 * Function hides all elements with '.content-box' class from page.
 */
function clearContentBoxes() {
  for (let element of document.getElementsByClassName('content-box')) {
    element.style.display = 'none';
  }
}

/**
 * Function displays all elements with given className on page.
 * @param className
 */
function loadContentBoxes(className) {
  for (let element of document.getElementsByClassName(className)) {
    element.style.display = 'flex';
  }
}

function updateArticle(heading, content) {
  document.getElementById('article-heading').innerHTML = heading;
  document.getElementById('article-content').innerHTML = content;
}

// Initialize today's Date
let todayDate = new Date();
// Initialize images
let pageLogoImg = document.getElementById('page-logo');
// Initialize constants
const RESULTS_NUM = 3;
const ADMIN_EMAIL = 'admin@email.cz';

async function loadMealDataOnce(createdMealId) {
  try {
    return await db.collection('meals').doc(createdMealId).get();
  } catch (error) {
    console.log('Error occurred during loading meals: ' + error);
  }
}

/**
 * Function for generating unique id.
 * @returns {string} Sting with randomized values.
 */
function getUniqueID() {
  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return '_' + Math.random().toString(36).substr(2, 9);
}

/**
 * Function adds listener on button for adding new meal record to user's meal records.
 */
function addCreatedMealButtonEventListener() {
  let divElement = document.getElementById('created-meal-buttons');

  let createdMealButton = document.createElement('button');
  createdMealButton.classList.add('content-button');
  createdMealButton.innerText = 'Zapsat';
  createdMealButton.id = 'record-meal-button';

  createdMealButton.addEventListener('click', function (e) {
    let textElementsToValidate = [
      'created-meal-name-input',
      'created-meal-calories-input',
      'created-meal-protein-input',
      'created-meal-fat-input',
      'created-meal-sugars-input',
    ];
    let numberElementsToValidate = ['created-meal-amount-input'];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }

    let selectElement = document.getElementById('meal-type');
    let mealType = selectElement.options[selectElement.selectedIndex].text;

    let mealRecord = {
      meal_id: document.getElementById('created-meal-id-input').value,
      amount: parseFloat(document.getElementById('created-meal-amount-input').value),
      meal_type: mealType,
      meal_record_id: getUniqueID(),
      date: today,
    };
    document.getElementById('created-meal-amount-input').value = ``;
    _user._eaten_meals.push(mealRecord);
    _user.saveUser();
    window.location.hash = '#state';
  });

  divElement.appendChild(createdMealButton);
}

/**
 * Function updates meal record in database.
 * @param mealId Id of meal created by Firestore.
 * @param name Name of meal created by author.
 * @param calories Meal calories value set by author.
 * @param protein Meal protein value set by author.
 * @param fat Meal fats value set by author.
 * @param sugars Meal sugars value set by author.
 * @param ean Meal EAN code value set by author.
 */
async function editMeal(mealId, name, calories, protein, fat, sugars, ean) {
  try {
    let response = await db
      .collection('meals')
      .doc(mealId)
      .update({
        name: name,
        calories: parseFloat(calories),
        protein: parseFloat(protein),
        fat: parseFloat(fat),
        sugars: parseFloat(sugars),
        ean: parseInt(ean),
      });
  } catch (error) {
    console.log('Error updating meal: ', error);
  }
}

/**
 * Function deletes meal record in database.
 * @param mealId Id of meal generated by Firestore.
 */
async function deleteMealById(mealId) {
  try {
    const response = await db.collection('meals').doc(mealId).delete();
    console.log('Meal with id = ' + mealId + ' successfully deleted!');
  } catch (error) {
    console.error('Error occurred during meal deleting: ' + error);
  }
}

/**
 * Function loads page with created meal content boxes.
 * @param createdMealId Meal record id created by getId function.
 */
function loadCreatedMealPage(createdMealId) {
  clearContentBoxes();
  loadContentBoxes('created-meal-boxes');
  updateArticle(
    'Přehled potraviny',
    'Zde je zobrazen přehled vybrané potraviny. Potravinu lze zapsat do svého denního příjmu. Autor ji může také editovat a smazat.',
  );
  document.getElementById('created-tags').innerHTML = '';
  pageLogoImg.src = 'images/food.png';

  // set page location hash
  window.location.hash = '#created_meal';

  // create QR code

  let element = document.getElementById('gr_code');
  element.innerHTML = ``;

  if (element.display === 'none') {
    element.display = 'inline';
  } else element.display = 'none';

  let qr = new QRCode(document.getElementById('gr_code'), {
    width: 175,
    height: 175,
  });

  let url = 'https://calory-calculator-13.web.app/?meal_id=' + createdMealId;

  qr.makeCode(url);

  // load meal data once
  loadMealDataOnce(createdMealId).then((response) => {
    let mealSnapshotData = response.data();
    document.getElementById('created-meal-name-input').value = mealSnapshotData.name;
    document.getElementById('created-meal-calories-input').value =
      mealSnapshotData.calories;
    document.getElementById('created-meal-protein-input').value =
      mealSnapshotData.protein;
    document.getElementById('created-meal-fat-input').value = mealSnapshotData.fat;
    document.getElementById('created-meal-sugars-input').value = mealSnapshotData.sugars;
    document.getElementById('created-meal-ean-input').value = mealSnapshotData.ean;
    document.getElementById('created-meal-amount-type-input').value =
      mealSnapshotData.amount;
    document.getElementById('created-meal-id-input').value = createdMealId;

    // Clear buttons
    let divElement = document.getElementById('created-meal-buttons');
    divElement.innerHTML = ``;

    // Add event listeners to created buttons
    addCreatedMealButtonEventListener();

    // Verify current user is author of created meal
    if (userAuth.uid === mealSnapshotData.author_id || userAuth.email === ADMIN_EMAIL) {
      // Set inputs readonly - false
      document.getElementById('created-meal-name-input').readOnly = false;
      document.getElementById('created-meal-calories-input').readOnly = false;
      document.getElementById('created-meal-protein-input').readOnly = false;
      document.getElementById('created-meal-fat-input').readOnly = false;
      document.getElementById('created-meal-sugars-input').readOnly = false;
      document.getElementById('created-meal-ean-input').readOnly = false;
      // Create edit and delete buttons
      let editMealButton = document.createElement('button');
      editMealButton.classList.add('content-button');
      editMealButton.innerText = 'Upravit';
      editMealButton.addEventListener('click', () => {
        editMeal(
          createdMealId,
          document.getElementById('created-meal-name-input').value,
          document.getElementById('created-meal-calories-input').value,
          document.getElementById('created-meal-protein-input').value,
          document.getElementById('created-meal-fat-input').value,
          document.getElementById('created-meal-sugars-input').value,
          document.getElementById('created-meal-ean-input').value,
        );
        window.location.hash = '#state';
      });

      let deleteMealButton = document.createElement('button');
      deleteMealButton.classList.add('content-button');
      deleteMealButton.innerText = 'Smazat';
      deleteMealButton.id = 'delete-meal-button';
      deleteMealButton.addEventListener('click', () => {
        deleteMealById(createdMealId);
        window.location.hash = '#state';
      });

      divElement.appendChild(editMealButton);
      divElement.appendChild(deleteMealButton);
    } else {
      // Set inputs readonly - true
      document.getElementById('created-meal-name-input').readOnly = true;
      document.getElementById('created-meal-calories-input').readOnly = true;
      document.getElementById('created-meal-protein-input').readOnly = true;
      document.getElementById('created-meal-fat-input').readOnly = true;
      document.getElementById('created-meal-sugars-input').readOnly = true;
      document.getElementById('created-meal-ean-input').readOnly = true;
    }

    for (let element of mealSnapshotData.tags) {
      let createdElement = document.createElement('label');
      createdElement.classList.add('tag');
      createdElement.innerText = element;
      createdElement.style.cursor = 'pointer';
      createdElement.addEventListener('click', () => {
        window.location = '#meals';
        document.getElementById('search-meal-input').value = element;
        document.getElementById('search_type').value = 'Tagu';
        document.getElementById('search-meal-button').click();
        document.getElementById('search-meal-button').scrollIntoView({
          behavior: 'smooth',
          block: 'end',
          inline: 'nearest',
        });
      });
      document.querySelector('#created-tags').appendChild(createdElement);
    }

    const url = new URL(window.location);
    window.history.pushState({}, document.title, url);
  });
}

/**
 * Class containing all information about meal record in database.
 */
class Meal {
  constructor(name, calories, protein, fat, sugars, ean, tags, amount, author_id) {
    this._name = name;
    this._calories = calories;
    this._protein = protein;
    this._fat = fat;
    this._sugars = sugars;
    this._ean = ean;
    this._tags = tags;
    this._amount = amount;
    this._author_id = author_id;
  }

  // Saves meal to Firestore
  async saveMeal() {
    try {
      let response = await db.collection('meals').add({
        name: this._name,
        calories: parseFloat(this._calories),
        protein: parseFloat(this._protein),
        fat: parseFloat(this._fat),
        sugars: parseFloat(this._sugars),
        ean: parseInt(this._ean),
        tags: this._tags,
        amount: this._amount,
        author_id: this._author_id,
      });
      console.log('Meal written with id = ', response.id);
      const createdMealId = response.id;
      loadCreatedMealPage(createdMealId);
    } catch (e) {
      console.error('Error adding meal: ', e);
    }
  }
}

function clearNewMealPage() {
  document.getElementById('meal-name-input').value = '';
  document.getElementById('meal-calories-input').value = '';
  document.getElementById('meal-protein-input').value = '';
  document.getElementById('meal-fat-input').value = '';
  document.getElementById('meal-sugars-input').value = '';
  document.getElementById('meal-ean-input').value = '';
  document.getElementById('new-meal-total-amount').value = '';
  document.querySelector('#added-tags').innerHTML = '';
}

const meal_button = document.getElementById('meal_button');

/**
 * Function adds listener on button for adding new meal to Firestore.
 */
function addNewMealButtonListener() {
  // Adding event listeners to button elements.
  meal_button.addEventListener('click', function (e) {
    e.preventDefault();

    let textElementsToValidate = ['meal-name-input'];
    let numberElementsToValidate = [
      'meal-calories-input',
      'meal-protein-input',
      'meal-fat-input',
      'meal-sugars-input',
    ];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }
    let ean = 0;
    let ean_input = document.getElementById('meal-ean-input').value;
    if (ean_input) {
      ean = ean_input;
    }
    let tags = [];
    for (let element of document.querySelector('#added-tags').childNodes) {
      tags.push(element.textContent);
    }
    let selectElement = document.getElementById('amount');
    let amount = selectElement.options[selectElement.selectedIndex].text;
    let newMeal = new Meal(
      document.getElementById('meal-name-input').value,
      document.getElementById('meal-calories-input').value,
      document.getElementById('meal-protein-input').value,
      document.getElementById('meal-fat-input').value,
      document.getElementById('meal-sugars-input').value,
      ean,
      tags,
      amount,
      userAuth.uid,
    );
    newMeal.saveMeal();
    clearNewMealPage();
  });
}

/**
 * Function deletes activity record in database.
 * @param activityId Id of an activity generated by Firestore.
 */
async function deleteActivityById(activityId) {
  try {
    const response = await db.collection('activities').doc(activityId).delete();
    console.log('Activity with id = ' + activityId + ' successfully deleted!');
  } catch (error) {
    console.error('Error occurred during activity deleting: ' + error);
  }
}

function resetLinksHighlights() {
  let elements = [];
  elements.push(document.getElementById('login-link'));
  elements.push(document.getElementById('logout-link'));
  elements.push(document.getElementById('activity-link'));
  elements.push(document.getElementById('meals-link'));
  elements.push(document.getElementById('newmeal-link'));
  elements.push(document.getElementById('settings-link'));
  elements.push(document.getElementById('state-link'));
  elements.push(document.getElementById('signup-link'));

  for (let element of elements) {
    element.classList.add('unselected');
    element.classList.remove('selected');
  }
}

function highlightLink(linkId) {
  document.getElementById(linkId).classList.remove('unselected');
  document.getElementById(linkId).classList.add('selected');
}

/**
 * Function sets article heading and content. Loads page with all activity boxes.
 */
function loadActivityPage() {
  clearContentBoxes();
  highlightLink('activity-link');
  loadContentBoxes('activity-boxes');
  updateArticle(
    'Aktivity',
    'V této sekci můžeš vytvořit novou aktivitu. Aktivitu si poté můžeš najít a zapsat. Stejně tak i ostatní uživatelé.',
  );
  document.getElementById('activity-tags').innerHTML = '';
  document.getElementById('search-activity-content-all').innerHTML = 'Výsledky hledání: ';
  pageLogoImg.src = 'images/activities.png';
}

/**
 * Function renders label element containing searched activity result description.
 * @param activityId
 * @param activityRecord
 */
function renderSearchedActivity(activityId, activityRecord) {
  let createdElement = document.createElement('label');
  const caloricOutcome = ((activityRecord.met_index * 3.5 * _user._weight) / 200) * 60;
  createdElement.classList.add('label-content');
  createdElement.classList.add('generated-content');
  createdElement.innerHTML = `<b style="font-size: 17px;">${activityRecord.name}</b><br>
     <br>Výdej kalorií : ${caloricOutcome.toFixed(0)} kcal / hod
     <br>Met index : ${activityRecord.met_index}<br>`;

  let createdInputDiv = document.createElement('div');
  createdInputDiv.classList.add('content_inline');
  createdInputDiv.style.display = 'none';
  createdElement.appendChild(createdInputDiv);

  let createdDiv = document.createElement('div');
  createdDiv.classList.add('content_inline');
  createdElement.appendChild(createdDiv);

  let createdLabel = document.createElement('label');
  createdLabel.innerText = 'Doba trvání [min]: ';
  createdLabel.style.height = '40px';
  createdInputDiv.appendChild(createdLabel);

  let createdInput = document.createElement('input');
  createdInput.type = 'number';
  createdInput.id = activityRecord.name + '-input';
  createdInputDiv.appendChild(createdInput);

  let confirmButton = document.createElement('button');
  confirmButton.classList.add('arrow-button');
  confirmButton.innerText = 'OK';
  confirmButton.id = activityRecord.name + '-confirm-button';
  createdInputDiv.appendChild(confirmButton);
  confirmButton.addEventListener('click', () => {
    if (createdInput.value === undefined || createdInput.value <= 0) {
      return;
    }
    let record = {
      name: activityRecord.name,
      duration: parseFloat(createdInput.value),
      met_index: activityRecord.met_index,
      activity_record_id: getUniqueID(),
      date: today,
    };
    _user._performed_activities.push(record);
    _user.saveUser();
    window.location.hash = '#state';
  });

  let createdAddButton = document.createElement('button');
  createdAddButton.classList.add('content-button');
  createdAddButton.innerText = 'Zapsat';
  createdAddButton.id = activityRecord.name + '-add-button';
  createdDiv.appendChild(createdAddButton);

  createdAddButton.addEventListener('click', function (e) {
    if (createdInputDiv.style.display === 'none') {
      createdInputDiv.style.display = 'initial';
      createdAddButton.innerText = 'Zrušit';
    } else {
      createdInputDiv.style.display = 'none';
      createdAddButton.innerText = 'Zapsat';
    }
  });

  if (activityRecord.author_id === userAuth.uid || userAuth.email === ADMIN_EMAIL) {
    let createdDeleteButton = document.createElement('button');
    createdDeleteButton.classList.add('content-button');
    createdDeleteButton.innerText = 'Smazat';
    createdDiv.appendChild(createdDeleteButton);

    createdDeleteButton.addEventListener('click', function (e) {
      deleteActivityById(activityId);
      loadActivityPage();
    });
  }

  document.getElementById('search-activity-content-all').appendChild(createdElement);
}

const search_activity_button = document.getElementById('search-activity-button');

/**
 * Function adds listener on button for searching an activity.
 *
 * Button's onclick function searches for results in database snapshot using fulltext
 * search. Results are then displayed on pages with fixed number of results.
 */
function addSearchActivityButtonListener() {
  search_activity_button.addEventListener('click', function (e) {
    let searchedText = document.getElementById('search-activity-input').value;
    let textElementsToValidate = ['search-activity-input'];
    if (!validate([], textElementsToValidate)) {
      return;
    }
    let selectElement = document.getElementById('activity_search_type');
    let searchType = selectElement.options[selectElement.selectedIndex].text;

    // get activities from db snapshot
    let activities = [];

    for (let doc of activitiesSnapshot) {
      if (searchType === 'Názvu') {
        const reg = new RegExp(searchedText, 'i');
        if (doc.data().name.match(reg)) {
          activities.push({ id: doc.id, data: doc.data() });
        }
      } else if (searchType === 'Tagu') {
        let reg = new RegExp(searchedText, 'i');
        for (let tag of doc.data().tags) {
          if (tag.match(reg)) {
            activities.push({ id: doc.id, data: doc.data() });
            break;
          }
        }
      }
    }

    // render activity pages
    let currentPage = 0;

    document.getElementById(
      'search-activity-content-all',
    ).innerHTML = `Výsledky hledání: "${searchedText}"`;

    let pagesCount = parseInt(activities.length / RESULTS_NUM);
    if (activities.length % RESULTS_NUM > 0) {
      pagesCount++;
    }

    let createdPagesDiv = document.createElement('div');
    createdPagesDiv.classList.add('content_inline_center');

    let createdPages = [];

    for (let i = 1; i <= pagesCount; i++) {
      let createdPage = document.createElement('label');
      createdPage.innerText = i.toString();
      createdPages.push(createdPage);
      createdPage.style.cursor = 'pointer';
      createdPage.addEventListener('click', () => {
        for (let page of createdPages) {
          if (page.innerText === createdPage.innerText) {
            page.style.fontSize = '18px';
          } else {
            page.style.fontSize = '14px';
          }
        }
        document.getElementById(
          'search-activity-content-all',
        ).innerHTML = `Výsledky hledání: "${searchedText}"`;
        document
          .getElementById('search-activity-content-all')
          .appendChild(createdPagesDiv);
        currentPage = parseInt(createdPage.innerText) - 1;
        // render current page
        for (
          let i = currentPage * RESULTS_NUM;
          i < RESULTS_NUM + currentPage * RESULTS_NUM;
          i++
        ) {
          if (i < activities.length) {
            renderSearchedActivity(activities[i].id, activities[i].data);
          }
        }
      });
      createdPagesDiv.appendChild(createdPage);
    }

    document.getElementById('search-activity-content-all').appendChild(createdPagesDiv);

    // render first page
    for (
      let i = currentPage * RESULTS_NUM;
      i < RESULTS_NUM + currentPage * RESULTS_NUM;
      i++
    ) {
      if (i < activities.length) {
        renderSearchedActivity(activities[i].id, activities[i].data);
      }
    }

    // show current page
    for (let page of createdPages) {
      if (page.innerText === '1') {
        page.style.fontSize = '18px';
      } else {
        page.style.fontSize = '14px';
      }
    }

    document.getElementById('search-activity-input').value = ``;
  });
}

/**
 * Function adds values to currently created meal. Values are arithmetically
 * averaged with previously added meals.
 * @param meal Object containing information about meal.
 * @param amount Amount of added meal (on 1g/ml or 100g/ml).
 */
function addToNewMeal(meal, amount) {
  let amountCoefficient = parseFloat(amount);
  let caloriesValue = 0;
  let proteinValue = 0;
  let fatValue = 0;
  let sugarsValue = 0;
  let totalAmount = 0;
  let newTotalAmount = 0;

  // update new total amount of meal
  if (
    !isNaN(document.getElementById('new-meal-total-amount').value) &&
    document.getElementById('new-meal-total-amount').value !== ''
  ) {
    totalAmount = parseFloat(document.getElementById('new-meal-total-amount').value);
  }
  newTotalAmount = totalAmount + parseFloat(amount);
  document.getElementById('new-meal-total-amount').value = newTotalAmount.toFixed(0);

  if (meal.amount === '100g' || meal.amount === '100ml') {
    amountCoefficient = parseFloat(amount) / 100;
  }
  if (
    !isNaN(document.getElementById('meal-calories-input').value) &&
    document.getElementById('meal-calories-input').value !== ''
  ) {
    caloriesValue = parseFloat(document.getElementById('meal-calories-input').value);
  }
  if (
    !isNaN(document.getElementById('meal-protein-input').value) &&
    document.getElementById('meal-protein-input').value !== ''
  ) {
    proteinValue = parseFloat(document.getElementById('meal-protein-input').value);
  }
  if (
    !isNaN(document.getElementById('meal-fat-input').value) &&
    document.getElementById('meal-fat-input').value !== ''
  ) {
    fatValue = parseFloat(document.getElementById('meal-fat-input').value);
  }
  if (
    !isNaN(document.getElementById('meal-sugars-input').value) &&
    document.getElementById('meal-sugars-input').value !== ''
  ) {
    sugarsValue = parseFloat(document.getElementById('meal-sugars-input').value);
  }

  document.getElementById('meal-calories-input').value = (
    (caloriesValue * (totalAmount / 100) + amountCoefficient * meal.calories) /
    (newTotalAmount / 100)
  ).toFixed(0);
  document.getElementById('meal-protein-input').value = (
    (proteinValue * (totalAmount / 100) + amountCoefficient * meal.protein) /
    (newTotalAmount / 100)
  ).toFixed(0);
  document.getElementById('meal-fat-input').value = (
    (fatValue * (totalAmount / 100) + amountCoefficient * meal.fat) /
    (newTotalAmount / 100)
  ).toFixed(0);
  document.getElementById('meal-sugars-input').value = (
    (sugarsValue * (totalAmount / 100) + amountCoefficient * meal.sugars) /
    (newTotalAmount / 100)
  ).toFixed(0);

  // add meal name as a new tag
  let createdElement = document.createElement('label');
  createdElement.classList.add('tag');
  createdElement.innerText = meal.name;
  document.querySelector('#added-tags').appendChild(createdElement);

  createdElement.addEventListener('click', () => {
    document.querySelector('#added-tags').removeChild(createdElement);
  });
}

/**
 * Function renders label element containing searched meal result description.
 * @param mealId
 * @param mealRecord
 */
function renderSearchedMeal(mealId, mealRecord) {
  let createdElement = document.createElement('label');
  createdElement.classList.add('label-content');
  createdElement.classList.add('generated-content');
  createdElement.innerHTML = `<b style="font-size: 17px;">${mealRecord.name}</b><br>
          <br>S hodnotami na ${mealRecord.amount}:
          <br>Kalorie : ${mealRecord.calories} kcal
          <br>Bílkoviny : ${mealRecord.protein} g
          <br>Tuky : ${mealRecord.fat} g
          <br>Sacharidy : ${mealRecord.sugars} g
          <br>`;

  // creating input element for adding values to new meal
  let createdInputDiv = document.createElement('div');
  createdInputDiv.classList.add('content_inline_flex_start');
  createdInputDiv.style.display = 'none';
  createdElement.appendChild(createdInputDiv);

  let createdInputLabel = document.createElement('label');
  createdInputLabel.innerText = 'Množství [g]: ';
  createdInputDiv.appendChild(createdInputLabel);

  let createdInput = document.createElement('input');
  createdInput.type = 'number';
  createdInput.id = getUniqueID();
  createdInputDiv.appendChild(createdInput);

  let confirmButton = document.createElement('button');
  confirmButton.classList.add('arrow-button');
  confirmButton.innerText = 'OK';
  createdInputDiv.appendChild(confirmButton);
  confirmButton.addEventListener('click', () => {
    if (!validateNumberInput(createdInput.id)) return;
    addToNewMeal(mealRecord, createdInput.value);
    window.location.hash = '#newmeal';
  });

  // adding show meal / add meal buttons
  let createdDiv = document.createElement('div');
  createdDiv.classList.add('content_inline');
  createdElement.appendChild(createdDiv);

  let showMealButton = document.createElement('button');
  showMealButton.classList.add('content-button');
  showMealButton.innerText = 'Zapsat';
  createdDiv.appendChild(showMealButton);

  let addToNewMealButton = document.createElement('button');
  addToNewMealButton.classList.add('content-button');
  addToNewMealButton.innerText = 'Přidat k novému';
  createdDiv.appendChild(addToNewMealButton);

  // adding event listeners to buttons
  showMealButton.addEventListener('click', function (e) {
    e.preventDefault();
    loadCreatedMealPage(mealId);
  });

  addToNewMealButton.addEventListener('click', function (e) {
    if (createdInputDiv.style.display === 'none') {
      createdInputDiv.style.display = 'initial';
      addToNewMealButton.innerText = 'Zrušit';
    } else {
      createdInputDiv.style.display = 'none';
      addToNewMealButton.innerText = 'Přidat k novému';
    }
  });

  document.getElementById('search-meal-content-all').appendChild(createdElement);
}

const search_meal_button = document.getElementById('search-meal-button');

/**
 * Function adds listener on button for searching a meal in Firestore.
 *
 * Button's onclick function searches for results in database snapshot using fulltext
 * search. Results of searched meals with brief description are then displayed
 * on pages with fixed number of results.
 */
function addSearchMealButtonEventListener() {
  search_meal_button.addEventListener('click', function (e) {
    let reg;
    let searchedText = document.getElementById('search-meal-input').value;
    let textElementsToValidate = ['search-meal-input'];
    if (!validate([], textElementsToValidate)) {
      return;
    }
    let selectElement = document.getElementById('search_type');
    let searchType = selectElement.options[selectElement.selectedIndex].text;

    // get meals from db snapshots
    let meals = [];

    if (searchType === 'Názvu') {
      for (let doc of mealsSnapshot) {
        reg = new RegExp(searchedText, 'i');
        if (doc.data().name.match(reg)) {
          meals.push({ id: doc.id, data: doc.data() });
        }
      }
    } else if (searchType === 'Tagu') {
      for (let doc of mealsSnapshot) {
        reg = new RegExp(searchedText, 'i');
        for (let tag of doc.data().tags) {
          if (tag.match(reg)) {
            meals.push({ id: doc.id, data: doc.data() });
            break;
          }
        }
      }
    } else if (searchType === 'Eanu') {
      for (let doc of mealsSnapshot) {
        if (doc.data().ean === parseInt(searchedText)) {
          meals.push({ id: doc.id, data: doc.data() });
        }
      }
    }

    // render meal pages
    let currentPage = 0;

    document.getElementById(
      'search-meal-content-all',
    ).innerHTML = `Výsledky hledání: "${searchedText}"`;

    let pagesCount = parseInt(meals.length / RESULTS_NUM);
    if (meals.length % RESULTS_NUM > 0) {
      pagesCount++;
    }

    let createdPagesDiv = document.createElement('div');
    createdPagesDiv.classList.add('content_inline_center');

    let createdPages = [];

    for (let i = 1; i <= pagesCount; i++) {
      let createdPage = document.createElement('label');
      createdPage.innerText = i.toString();
      createdPages.push(createdPage);
      createdPage.style.cursor = 'pointer';
      createdPage.addEventListener('click', () => {
        for (let page of createdPages) {
          if (page.innerText === createdPage.innerText) {
            page.style.fontSize = '18px';
          } else {
            page.style.fontSize = '14px';
          }
        }
        document.getElementById(
          'search-meal-content-all',
        ).innerHTML = `Výsledky hledání: "${searchedText}"`;
        document.getElementById('search-meal-content-all').appendChild(createdPagesDiv);
        currentPage = parseInt(createdPage.innerText) - 1;
        // render current page
        for (
          let i = currentPage * RESULTS_NUM;
          i < RESULTS_NUM + currentPage * RESULTS_NUM;
          i++
        ) {
          if (i < meals.length) {
            renderSearchedMeal(meals[i].id, meals[i].data);
          }
        }
      });
      createdPagesDiv.appendChild(createdPage);
    }

    document.getElementById('search-meal-content-all').appendChild(createdPagesDiv);

    // render first page
    for (
      let i = currentPage * RESULTS_NUM;
      i < RESULTS_NUM + currentPage * RESULTS_NUM;
      i++
    ) {
      if (i < meals.length) {
        renderSearchedMeal(meals[i].id, meals[i].data);
      }
    }

    // show current page
    for (let page of createdPages) {
      if (page.innerText === '1') {
        page.style.fontSize = '18px';
      } else {
        page.style.fontSize = '14px';
      }
    }

    document.getElementById('search-meal-input').value = ``;
  });
}

/**
 * Class for storing all the data about user.
 */
class User {
  constructor(
    name,
    weight,
    height,
    year_of_birth,
    sex,
    activity,
    goal,
    eaten_meals,
    performed_activities,
  ) {
    this._name = name;
    this._weight = weight;
    this._height = height;
    this._year_of_birth = year_of_birth;
    this._sex = sex;
    this._activity = activity;
    this._goal = goal;
    this._basal = 0.01;
    this._recommended_income_protein = 0.01;
    this._recommended_income_fat = 0.01;
    this._recommended_income_sugars = 0.01;
    this._eaten_meals = eaten_meals;
    this._performed_activities = performed_activities;
    this._age = this.calculateAge(new Date(year_of_birth, 1, 1));
    this.calculateUsersOutcome(today);
  }

  /**
   * Function calculates user's age from date of birth and current date.
   * @param dob Date of birth
   * @returns {number} Age
   */
  calculateAge(dob) {
    let diff_ms = Date.now() - dob.getTime();
    let age_dt = new Date(diff_ms);

    return Math.abs(age_dt.getUTCFullYear() - 1970);
  }

  /**
   * Function calculate user's total calories outcome for specific day based on
   * his basal metabolism and performed activities.
   * @param day Day specified in 'dd/mm/yyyy' format
   */
  calculateUsersOutcome(day) {
    let newBasal = 0.0;
    if (this._sex === 'female') {
      newBasal =
        655.0955 + 9.5634 * this._weight + 1.8496 * this._height - 4.6756 * this._age;
    } else if (this._sex === 'male') {
      newBasal =
        66.473 + 13.7516 * this._weight + 5.0033 * this._height - 6.755 * this._age;
    }
    this._basal = newBasal * this._activity * this._goal;

    for (let activity of this._performed_activities) {
      if (activity.date === day) {
        this._basal +=
          ((activity.met_index * 3.5 * this._weight) / 200) * activity.duration;
      }
    }

    this._recommended_income_protein = this._basal * 0.075;
    this._recommended_income_fat = this._basal * 0.033;
    this._recommended_income_sugars = this._basal * 0.1;
  }

  get basal() {
    return this._basal;
  }

  /**
   * Function saves user's data to database.
   */
  async saveUser() {
    try {
      const response = await db
        .collection('users')
        .doc(userAuth.uid)
        .set({
          name: this._name,
          weight: parseFloat(this._weight),
          height: parseFloat(this._height),
          year_of_birth: parseInt(this._year_of_birth),
          sex: this._sex,
          activity: parseFloat(this._activity),
          goal: parseFloat(this._goal),
          eaten_meals: this._eaten_meals,
          performed_activities: this._performed_activities,
        });
      console.log('User data successfully written!');
    } catch (error) {
      console.log('Error writing document: ', error);
    }
  }
}

/**
 * Function generates random color hex using Math.random() function.
 * @returns {string} Random color hex-code.
 */
function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

// update user content box

function updateUserBox() {
  if (userAuth) {
    // create SVG with username
    let svg = document.getElementById('svg-username');
    let svgNs = 'http://www.w3.org/2000/svg';
    svg.innerHTML = ``;

    // create circle element
    let circleElement = document.createElementNS(svgNs, 'circle');
    circleElement.setAttributeNS(null, 'cx', '50');
    circleElement.setAttributeNS(null, 'cy', '50');
    circleElement.setAttributeNS(null, 'r', '50');
    circleElement.setAttributeNS(null, 'fill', getRandomColor()); //'#aeaeae' or getRandomColor()

    // create text element
    let textElement = document.createElementNS(svgNs, 'text');
    textElement.setAttributeNS(null, 'x', '50%');
    textElement.setAttributeNS(null, 'y', '50%');
    textElement.setAttributeNS(null, 'fill', 'white');
    textElement.setAttributeNS(null, 'text-anchor', 'middle');
    textElement.setAttributeNS(null, 'font-size', '50px');
    textElement.setAttributeNS(null, 'font-family', 'Arial');
    textElement.setAttributeNS(null, 'dy', '.35em');
    textElement.textContent = _user._name[0];

    // append elements to svg object
    svg.appendChild(circleElement);
    svg.appendChild(textElement);

    document.getElementById('user-name').innerText =
      ' Uživatel: ' +
      _user._name +
      '\r' +
      ' Rok narození: ' +
      _user._year_of_birth +
      '\r' +
      ' Hmotnost: ' +
      _user._weight +
      ' kg' +
      '\r \r' +
      ' Denní příjem: ';
  }
}

const basal_button = document.getElementById('basal_button');

/**
 * Function adds listener on button for updating user settings.
 *
 * Button's onclick function saves user's information to Firestore and loads
 * state page.
 */
function addSettingsButtonEventListener() {
  basal_button.addEventListener('click', function (e) {
    e.preventDefault();
    let textElementsToValidate = ['name-input'];
    let numberElementsToValidate = [
      'weight-input',
      'height-input',
      'year-of-birth-input',
    ];
    if (!validate(numberElementsToValidate, textElementsToValidate)) {
      return;
    }
    _user = new User(
      document.getElementById('name-input').value,
      document.getElementById('weight-input').value,
      document.getElementById('height-input').value,
      document.getElementById('year-of-birth-input').value,
      document.querySelector('.sex:checked').value,
      document.querySelector('.activity:checked').value,
      document.querySelector('.goal:checked').value,
      _user._eaten_meals,
      _user._performed_activities,
    );
    _user.saveUser();
    updateUserBox();
    window.location.hash = '#state';
  });
}

const add_meal_tag_button = document.getElementById('add-meal-tag-button');

/**
 * Function adds listener on button for adding new tag to currently created meal.
 */
function addMealTagButtonEventListener() {
  add_meal_tag_button.addEventListener('click', function (e) {
    let textElementsToValidate = ['add-meal-tag'];
    if (!validate([], textElementsToValidate)) {
      return;
    }

    let createdElement = document.createElement('label');
    createdElement.classList.add('tag');
    createdElement.innerText = document.getElementById('add-meal-tag').value;
    document.getElementById('add-meal-tag').value = '';
    document.querySelector('#added-tags').appendChild(createdElement);

    createdElement.addEventListener('click', () => {
      document.querySelector('#added-tags').removeChild(createdElement);
    });
  });
}

class e {
  static hasCamera() {
    return navigator.mediaDevices
      ? navigator.mediaDevices
          .enumerateDevices()
          .then((a) => a.some((a) => 'videoinput' === a.kind))
          .catch(() => !1)
      : Promise.resolve(!1);
  }
  constructor(
    a,
    b,
    c = this._onDecodeError,
    d = this._calculateScanRegion,
    f = 'environment',
  ) {
    this.$video = a;
    this.$canvas = document.createElement('canvas');
    this._onDecode = b;
    this._legacyCanvasSize = e.DEFAULT_CANVAS_SIZE;
    this._preferredFacingMode = f;
    this._flashOn = this._paused = this._active = !1;
    'number' === typeof c
      ? ((this._legacyCanvasSize = c),
        console.warn(
          "You're using a deprecated version of the QrScanner constructor which will be removed in the future",
        ))
      : (this._onDecodeError = c);
    'number' === typeof d
      ? ((this._legacyCanvasSize = d),
        console.warn(
          "You're using a deprecated version of the QrScanner constructor which will be removed in the future",
        ))
      : (this._calculateScanRegion = d);
    this._scanRegion = this._calculateScanRegion(a);
    this._onPlay = this._onPlay.bind(this);
    this._onLoadedMetaData = this._onLoadedMetaData.bind(this);
    this._onVisibilityChange = this._onVisibilityChange.bind(this);
    this.$video.playsInline = !0;
    this.$video.muted = !0;
    this.$video.disablePictureInPicture = !0;
    this.$video.addEventListener('play', this._onPlay);
    this.$video.addEventListener('loadedmetadata', this._onLoadedMetaData);
    document.addEventListener('visibilitychange', this._onVisibilityChange);
    this._qrEnginePromise = e.createQrEngine();
  }
  hasFlash() {
    if (!('ImageCapture' in window)) return Promise.resolve(!1);
    let a = this.$video.srcObject ? this.$video.srcObject.getVideoTracks()[0] : null;
    return a
      ? new ImageCapture(a)
          .getPhotoCapabilities()
          .then((a) => a.fillLightMode.includes('flash'))
          .catch((a) => {
            console.warn(a);
            return !1;
          })
      : Promise.reject('Camera not started or not available');
  }
  isFlashOn() {
    return this._flashOn;
  }
  toggleFlash() {
    return this._setFlash(!this._flashOn);
  }
  turnFlashOff() {
    return this._setFlash(!1);
  }
  turnFlashOn() {
    return this._setFlash(!0);
  }
  destroy() {
    this.$video.removeEventListener('loadedmetadata', this._onLoadedMetaData);
    this.$video.removeEventListener('play', this._onPlay);
    document.removeEventListener('visibilitychange', this._onVisibilityChange);
    this.stop();
    e._postWorkerMessage(this._qrEnginePromise, 'close');
  }
  start() {
    if (this._active && !this._paused) return Promise.resolve();
    'https:' !== window.location.protocol &&
      console.warn(
        'The camera stream is only accessible if the page is transferred via https.',
      );
    this._active = !0;
    this._paused = !1;
    if (document.hidden) return Promise.resolve();
    clearTimeout(this._offTimeout);
    this._offTimeout = null;
    if (this.$video.srcObject) return this.$video.play(), Promise.resolve();
    let a = this._preferredFacingMode;
    return this._getCameraStream(a, !0)
      .catch(() => {
        a = 'environment' === a ? 'user' : 'environment';
        return this._getCameraStream();
      })
      .then((b) => {
        a = this._getFacingMode(b) || a;
        this.$video.srcObject = b;
        this.$video.play();
        this._setVideoMirror(a);
      })
      .catch((a) => {
        this._active = !1;
        throw a;
      });
  }
  stop() {
    this.pause();
    this._active = !1;
  }
  pause() {
    this._paused = !0;
    this._active &&
      (this.$video.pause(),
      this._offTimeout ||
        (this._offTimeout = setTimeout(() => {
          let a = this.$video.srcObject ? this.$video.srcObject.getTracks() : [];
          for (let b of a) b.stop();
          this._offTimeout = this.$video.srcObject = null;
        }, 300)));
  }
  static scanImage(a, b = null, c = null, d = null, f = !1, k = !1) {
    let h = c instanceof Worker,
      g = Promise.all([c || e.createQrEngine(), e._loadImage(a)]).then(([a, g]) => {
        c = a;
        let k;
        [d, k] = this._drawToCanvas(g, b, d, f);
        return c instanceof Worker
          ? (h || c.postMessage({ type: 'inversionMode', data: 'both' }),
            new Promise((a, b) => {
              let f, l, g;
              l = (d) => {
                'qrResult' === d.data.type &&
                  (c.removeEventListener('message', l),
                  c.removeEventListener('error', g),
                  clearTimeout(f),
                  null !== d.data.data ? a(d.data.data) : b(e.NO_QR_CODE_FOUND));
              };
              g = (a) => {
                c.removeEventListener('message', l);
                c.removeEventListener('error', g);
                clearTimeout(f);
                b('Scanner error: ' + (a ? a.message || a : 'Unknown Error'));
              };
              c.addEventListener('message', l);
              c.addEventListener('error', g);
              f = setTimeout(() => g('timeout'), 1e4);
              let h = k.getImageData(0, 0, d.width, d.height);
              c.postMessage({ type: 'decode', data: h }, [h.data.buffer]);
            }))
          : new Promise((a, b) => {
              let f = setTimeout(() => b('Scanner error: timeout'), 1e4);
              c.detect(d)
                .then((c) => {
                  c.length ? a(c[0].rawValue) : b(e.NO_QR_CODE_FOUND);
                })
                .catch((a) => b('Scanner error: ' + (a.message || a)))
                .finally(() => clearTimeout(f));
            });
      });
    b && k && (g = g.catch(() => e.scanImage(a, null, c, d, f)));
    return (g = g.finally(() => {
      h || e._postWorkerMessage(c, 'close');
    }));
  }
  setGrayscaleWeights(a, b, c, d = !0) {
    e._postWorkerMessage(this._qrEnginePromise, 'grayscaleWeights', {
      red: a,
      green: b,
      blue: c,
      useIntegerApproximation: d,
    });
  }
  setInversionMode(a) {
    e._postWorkerMessage(this._qrEnginePromise, 'inversionMode', a);
  }
  static createQrEngine(a = e.WORKER_PATH) {
    return ('BarcodeDetector' in window
      ? BarcodeDetector.getSupportedFormats()
      : Promise.resolve([])
    ).then((b) =>
      -1 !== b.indexOf('qr_code')
        ? new BarcodeDetector({ formats: ['qr_code'] })
        : new Worker(a),
    );
  }
  _onPlay() {
    this._scanRegion = this._calculateScanRegion(this.$video);
    this._scanFrame();
  }
  _onLoadedMetaData() {
    this._scanRegion = this._calculateScanRegion(this.$video);
  }
  _onVisibilityChange() {
    document.hidden ? this.pause() : this._active && this.start();
  }
  _calculateScanRegion(a) {
    let b = Math.round((2 / 3) * Math.min(a.videoWidth, a.videoHeight));
    return {
      x: (a.videoWidth - b) / 2,
      y: (a.videoHeight - b) / 2,
      width: b,
      height: b,
      downScaledWidth: this._legacyCanvasSize,
      downScaledHeight: this._legacyCanvasSize,
    };
  }
  _scanFrame() {
    if (!this._active || this.$video.paused || this.$video.ended) return !1;
    requestAnimationFrame(() => {
      1 >= this.$video.readyState
        ? this._scanFrame()
        : this._qrEnginePromise
            .then((a) => e.scanImage(this.$video, this._scanRegion, a, this.$canvas))
            .then(this._onDecode, (a) => {
              this._active &&
                (-1 !== (a.message || a).indexOf('service unavailable') &&
                  (this._qrEnginePromise = e.createQrEngine()),
                this._onDecodeError(a));
            })
            .then(() => this._scanFrame());
    });
  }
  _onDecodeError(a) {
    a !== e.NO_QR_CODE_FOUND && console.log(a);
  }
  _getCameraStream(a, b = !1) {
    let c = [{ width: { min: 1024 } }, { width: { min: 768 } }, {}];
    a && (b && (a = { exact: a }), c.forEach((b) => (b.facingMode = a)));
    return this._getMatchingCameraStream(c);
  }
  _getMatchingCameraStream(a) {
    return navigator.mediaDevices && 0 !== a.length
      ? navigator.mediaDevices
          .getUserMedia({ video: a.shift() })
          .catch(() => this._getMatchingCameraStream(a))
      : Promise.reject('Camera not found.');
  }
  _setFlash(a) {
    return this.hasFlash()
      .then((b) =>
        b
          ? this.$video.srcObject
              .getVideoTracks()[0]
              .applyConstraints({ advanced: [{ torch: a }] })
          : Promise.reject('No flash available'),
      )
      .then(() => (this._flashOn = a));
  }
  _setVideoMirror(a) {
    this.$video.style.transform = 'scaleX(' + ('user' === a ? -1 : 1) + ')';
  }
  _getFacingMode(a) {
    return (a = a.getVideoTracks()[0])
      ? /rear|back|environment/i.test(a.label)
        ? 'environment'
        : /front|user|face/i.test(a.label)
        ? 'user'
        : null
      : null;
  }
  static _drawToCanvas(a, b = null, c = null, d = !1) {
    c = c || document.createElement('canvas');
    let f = b && b.x ? b.x : 0,
      k = b && b.y ? b.y : 0,
      h = b && b.width ? b.width : a.width || a.videoWidth,
      g = b && b.height ? b.height : a.height || a.videoHeight;
    d ||
      ((c.width = b && b.downScaledWidth ? b.downScaledWidth : h),
      (c.height = b && b.downScaledHeight ? b.downScaledHeight : g));
    b = c.getContext('2d', { alpha: !1 });
    b.imageSmoothingEnabled = !1;
    b.drawImage(a, f, k, h, g, 0, 0, c.width, c.height);
    return [c, b];
  }
  static _loadImage(a) {
    if (
      a instanceof HTMLCanvasElement ||
      a instanceof HTMLVideoElement ||
      (window.ImageBitmap && a instanceof window.ImageBitmap) ||
      (window.OffscreenCanvas && a instanceof window.OffscreenCanvas)
    )
      return Promise.resolve(a);
    if (a instanceof Image) return e._awaitImageLoad(a).then(() => a);
    if (
      a instanceof File ||
      a instanceof Blob ||
      a instanceof URL ||
      'string' === typeof a
    ) {
      let b = new Image();
      b.src = a instanceof File || a instanceof Blob ? URL.createObjectURL(a) : a;
      return e._awaitImageLoad(b).then(() => {
        (a instanceof File || a instanceof Blob) && URL.revokeObjectURL(b.src);
        return b;
      });
    }
    return Promise.reject('Unsupported image type.');
  }
  static _awaitImageLoad(a) {
    return new Promise((b, c) => {
      if (a.complete && 0 !== a.naturalWidth) b();
      else {
        let d, f;
        d = () => {
          a.removeEventListener('load', d);
          a.removeEventListener('error', f);
          b();
        };
        f = () => {
          a.removeEventListener('load', d);
          a.removeEventListener('error', f);
          c('Image load error');
        };
        a.addEventListener('load', d);
        a.addEventListener('error', f);
      }
    });
  }
  static _postWorkerMessage(a, b, c) {
    return Promise.resolve(a).then((a) => {
      a instanceof Worker && a.postMessage({ type: b, data: c });
    });
  }
}
e.DEFAULT_CANVAS_SIZE = 400;
e.NO_QR_CODE_FOUND = 'No QR code found';
e.WORKER_PATH = 'qr-scanner-worker.min.js';

let qrScanner = null;
let isScanning = false;

/**
 * Function creates new QrScanner object and tries to access camera for scanning
 * and triggers QrScanner start() function.
 */
function startScanning() {
  isScanning = true;
  e.WORKER_PATH = 'scripts/qr_code_scanner/qr-scanner-worker.min.js';
  qrScanner = new e(document.getElementById('qr-video'), (result) => {
    let parsedResult = result.substring(46);
    if (parsedResult.length > 0) {
      qrScanner.stop();
      loadCreatedMealPage(parsedResult);
    } else {
      stopScanning();
      alert('Jídlo s uvedeným QR kódem nelze nalézt!');
      scanning_start_button.innerText = 'Start';
      scanning_start_button.style.backgroundColor = '#2fd015';
    }
  });
  qrScanner.start();
}

/**
 * Function triggers QrScanner stop() function.
 */
function stopScanning() {
  isScanning = false;
  qrScanner.stop();
}

const scanning_start_button = document.getElementById('scanning-start-button');

/**
 * Function adds listener on QR code scanning button.
 */
function addScanningStartButtonListener() {
  scanning_start_button.addEventListener('click', function () {
    if (!isScanning) {
      startScanning();
      scanning_start_button.innerText = 'Stop';
      scanning_start_button.style.backgroundColor = 'red';
    } else {
      stopScanning();
      scanning_start_button.innerText = 'Start';
      scanning_start_button.style.backgroundColor = '#2fd015';
    }
  });
}

const qr_search_button = document.getElementById('qr-code-search-logo');

/**
 * Function adds listener on button for searching with QR scanner.
 * Button's onclick function redirects user.
 */
function addQrSearchButtonListener() {
  qr_search_button.addEventListener('click', function (e) {
    window.location.hash = 'meals';
    if (userAuth) {
      scanning_start_button.focus();
      scanning_start_button.scrollIntoView({
        behavior: 'smooth',
        block: 'end',
        inline: 'nearest',
      });
    }
  });
}

const new_meal_cancel_button = document.getElementById('new_meal_cancel_button');

/**
 * Function adds listener to button for clearing 'create new meal' form.
 */
function addNewMealCancelButtonEventListener() {
  new_meal_cancel_button.addEventListener('click', () => {
    clearNewMealPage();
  });
}

const add_existing_meal_button = document.getElementById('add-existing-meal');

/**
 * Function adds listener on button for adding existing meal to newly created.
 */
function addAddExistingMealButtonListener() {
  add_existing_meal_button.addEventListener('click', () => {
    window.location.hash = '#meals';
    document
      .getElementById('search-meal-input')
      .scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    document.getElementById('search-meal-input').focus({ preventScroll: false });
  });
}

/**
 * Function initializes all static buttons with event listeners in application.
 */
function initializeAllButtonListeners() {
  addLoginFormEventListener();
  addSignUpFormEventListener();
  addActivityButtonEventListener();
  addNewActivityTagButtonEventListener();
  addNewMealButtonListener();
  addSearchActivityButtonListener();
  addSearchMealButtonEventListener();
  addSettingsButtonEventListener();
  addMealTagButtonEventListener();
  addScanningStartButtonListener();
  addQrSearchButtonListener();
  addNewMealCancelButtonEventListener();
  addAddExistingMealButtonListener();
}

// Loads login page with login form

/**
 * Function loads page with login content boxes.
 */
function loadLoginPage() {
  clearContentBoxes();
  loadContentBoxes('login-boxes');
  highlightLink('login-link');
  updateArticle(
    'Přihlášení uživatele',
    `Přihlaš se. Pokud nemáš vytvořený účet, přejdi na <a href="#signup" style="text-decoration: none; color: #303030"><b>registraci</b></a>.`,
  );
  pageLogoImg.src = 'images/user_login.png';
}

function displayDropdown() {
  document.getElementById('dropdown-div').style.display = 'inline-block';
}

function hideDropdown() {
  document.getElementById('dropdown-div').style.display = 'none';
}

/**
 * Function parses parameters from page's url.
 * @param url Page's url address.
 * @returns {{}} Array of parsed parameters.
 */
function getParams(url) {
  const params = {};
  const parser = document.createElement('a');
  parser.href = url;
  const query = parser.search.substring(1);
  const vars = query.split('&');
  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=');
    params[pair[0]] = decodeURIComponent(pair[1]);
  }
  return params;
}

/**
   Class for rendering percentage donut charts via SVG path elements.
*/
class SVGPieChart {
  constructor(svgElementId, size) {
    this._svg = document.querySelector(svgElementId);
    this._svgNs = 'http://www.w3.org/2000/svg';
    this._size = size;
  }

  /**
   * Function draws path element curved as a circle to create a donut chart
   * with full diameter.
   */
  drawBigChart(percentage, color, customChart, content) {
    let textContent = percentage;

    if (percentage === null) {
      percentage = 0;
    }
    if (percentage > 100) {
      percentage = 100;
    }
    let angle = percentage * 3.6;

    let pathElement = document.createElementNS(this._svgNs, 'path');
    pathElement.setAttributeNS(null, 'd', this.setArc(100, 100, angle));
    pathElement.setAttributeNS(null, 'stroke-width', '50');
    pathElement.setAttributeNS(null, 'stroke', color);
    pathElement.setAttributeNS(null, 'fill', 'none');

    let textElement = document.createElementNS(this._svgNs, 'text');
    textElement.setAttributeNS(null, 'x', '100');
    textElement.setAttributeNS(null, 'y', '100');
    textElement.setAttributeNS(null, 'fill', 'black');
    textElement.setAttributeNS(null, 'height', '40');
    textElement.setAttributeNS(null, 'text-anchor', 'middle');

    if (customChart) {
      textElement.textContent = textContent.toFixed(0) + ' % ' + content;
    }

    this._svg.appendChild(pathElement);
    this._svg.appendChild(textElement);
  }

  /**
   * Function draws smaller donut charge with half diameter.
   */
  drawSmallChart(percentage, color, customChart, content) {
    let textContent = percentage;

    if (percentage === null) {
      percentage = 0;
    }
    if (percentage > 100) {
      percentage = 100;
    }
    let angle = percentage * 3.6;

    let pathElement = document.createElementNS(this._svgNs, 'path');
    pathElement.setAttributeNS(null, 'd', this.setArc(50, 50, angle));
    pathElement.setAttributeNS(null, 'stroke-width', '20');
    pathElement.setAttributeNS(null, 'stroke', color);
    pathElement.setAttributeNS(null, 'fill', 'none');

    let textElement = document.createElementNS(this._svgNs, 'text');
    textElement.setAttributeNS(null, 'x', '50');
    textElement.setAttributeNS(null, 'y', '50');
    textElement.setAttributeNS(null, 'fill', 'black');
    textElement.setAttributeNS(null, 'height', '12');
    textElement.setAttributeNS(null, 'font-size', '11');
    textElement.setAttributeNS(null, 'text-anchor', 'middle');

    if (customChart) {
      textElement.textContent = textContent.toFixed(0) + '% ' + content;
    }

    this._svg.appendChild(pathElement);
    this._svg.appendChild(textElement);
  }

  /**
   * Function returns arc attributes for a path element.
   */
  setArc(x, y, angleTo) {
    let start = this.getCartesianCoordinates(x, y, angleTo);
    let end = this.getCartesianCoordinates(x, y, 0);

    let arcFlag = 0;
    if (angleTo >= 180) {
      arcFlag = 1;
    }

    return `M ${start.x} ${start.y} A ${x} ${y} 0 ${arcFlag} 0 ${end.x} ${end.y}`;
  }

  /**
   * Function returns coordinates to specific angle from center of coordinate system.
   */
  getCartesianCoordinates(centerX, centerY, angle) {
    let angleInRadians = ((angle - 90) * 3.14159) / 180.0;

    return {
      x: centerX + centerX * Math.cos(angleInRadians),
      y: centerY + centerY * Math.sin(angleInRadians),
    };
  }

  /**
   * Function resets SVG innerHTML content.
   */
  clear() {
    this._svg.innerHTML = '';
  }
}

/*
   Loads state page elements. Dumps user's income and basal values and all today's meals.
   Based on income / outcome (basal) ratio creates SVGPieChart of today's income percentage.
*/

/**
 * Function renders donut chart data describing today's income using SVGPieChart object.
 * @param day Date in 'dd/mm/yyyy' format
 */
function loadPieChartData(day) {
  // Recalculate users outcome
  _user.calculateUsersOutcome(day);

  // CALORIES CHART
  document.getElementById('pie-chart').style.display = 'initial';
  if (pieChart === null) {
    pieChart = new SVGPieChart('#pie-chart');
  } else {
    pieChart.clear();
  }

  let percentage_for_calories = (caloriesIncome / _user.basal) * 100; // pocet kalorii ku basalu

  pieChart.drawBigChart(100, 'grey', false);
  if (percentage_for_calories < 50) {
    pieChart.drawBigChart(percentage_for_calories, 'lime', true, 'kcal');
  } else if (percentage_for_calories < 75) {
    pieChart.drawBigChart(percentage_for_calories, 'orange', true, 'kcal');
  } else {
    pieChart.drawBigChart(percentage_for_calories, 'red', true, 'kcal');
  }

  // USER-BOX CHART
  document.getElementById('pie-chart-user-box').style.display = 'initial';
  if (pieChartUserBox === null) {
    pieChartUserBox = new SVGPieChart('#pie-chart-user-box');
  } else {
    pieChartUserBox.clear();
  }
  pieChartUserBox.drawSmallChart(100, 'grey', false);
  if (percentage_for_calories < 50) {
    pieChartUserBox.drawSmallChart(percentage_for_calories, 'lime', true, '[kcal]');
  } else if (percentage_for_calories < 75) {
    pieChartUserBox.drawSmallChart(percentage_for_calories, 'orange', true, '[kcal]');
  } else {
    pieChartUserBox.drawSmallChart(percentage_for_calories, 'red', true, '[kcal]');
  }

  // PROTEIN CHART
  let percentage_for_protein = (proteinIncome / _user._recommended_income_protein) * 100;

  document.getElementById('pie-chart-protein').style.display = 'initial';
  if (pieChartProtein === null) {
    pieChartProtein = new SVGPieChart('#pie-chart-protein');
  } else {
    pieChartProtein.clear();
  }
  pieChartProtein.drawSmallChart(100, 'grey', false);
  if (percentage_for_protein < 50) {
    pieChartProtein.drawSmallChart(percentage_for_protein, 'lime', true, 'bílkovin');
  } else if (percentage_for_protein < 75) {
    pieChartProtein.drawSmallChart(percentage_for_protein, 'orange', true, 'bílkovin');
  } else {
    pieChartProtein.drawSmallChart(percentage_for_protein, 'red', true, 'bílkovin');
  }

  // FAT CHART
  let percentage_for_fat = (fatIncome / _user._recommended_income_fat) * 100;

  document.getElementById('pie-chart-protein').style.display = 'initial';
  if (pieChartFat === null) {
    pieChartFat = new SVGPieChart('#pie-chart-fat');
  } else {
    pieChartFat.clear();
  }
  pieChartFat.drawSmallChart(100, 'grey', false);
  if (percentage_for_fat < 50) {
    pieChartFat.drawSmallChart(percentage_for_fat, 'lime', true, 'tuků');
  } else if (percentage_for_fat < 75) {
    pieChartFat.drawSmallChart(percentage_for_fat, 'orange', true, 'tuků');
  } else {
    pieChartFat.drawSmallChart(percentage_for_fat, 'red', true, 'tuků');
  }

  // SUGARS CHART
  let percentage_for_sugars = (sugarsIncome / _user._recommended_income_sugars) * 100;

  document.getElementById('pie-chart-protein').style.display = 'initial';
  if (pieChartSugars === null) {
    pieChartSugars = new SVGPieChart('#pie-chart-sugars');
  } else {
    pieChartSugars.clear();
  }
  pieChartSugars.drawSmallChart(100, 'grey', false);
  if (percentage_for_sugars < 50) {
    pieChartSugars.drawSmallChart(percentage_for_sugars, 'lime', true, 'sacharidů');
  } else if (percentage_for_sugars < 75) {
    pieChartSugars.drawSmallChart(percentage_for_sugars, 'orange', true, 'sacharidů');
  } else {
    pieChartSugars.drawSmallChart(percentage_for_sugars, 'red', true, 'sacharidů');
  }

  document.getElementById('calories-income-label').innerHTML =
    'Kalorie <br><b>' +
    caloriesIncome.toFixed(0) +
    ' kcal </b> z ' +
    _user.basal.toFixed(0) +
    ' kcal';
  document.getElementById('protein-income-label').innerHTML =
    'Bílkoviny <br><b>' +
    proteinIncome.toFixed(0) +
    ' g </b> z ' +
    _user._recommended_income_protein.toFixed(0) +
    ' g <br>';
  document.getElementById('fats-income-label').innerHTML =
    'Tuky <br><b>' +
    fatIncome.toFixed(0) +
    ' g </b> z ' +
    _user._recommended_income_fat.toFixed(0) +
    ' g <br>';
  document.getElementById('sugars-income-label').innerHTML =
    'Sacharidy <br><b>' +
    sugarsIncome.toFixed(0) +
    ' g </b> z ' +
    _user._recommended_income_sugars.toFixed(0) +
    ' g <br>';
}

/**
 * Function renders element in state page box content, containing description
 * of an activity performed by user in given day.
 *
 * @param activity Object containing information about activity record
 * @param day Date in 'dd/mm/yyyy' format
 */
function renderActivity(activity, day) {
  if (activity.date !== day) {
    return;
  }
  const caloricOutcome =
    ((activity.met_index * 3.5 * _user._weight) / 200) * activity.duration;
  let createdElement = document.createElement('label');
  createdElement.classList.add('label-content');
  createdElement.classList.add('generated-content');
  createdElement.innerHTML = `<b style="font-size: 17px;">${activity.name}</b><br>
     <br>Výdej kalorií : ${caloricOutcome.toFixed(0)} kcal
     <br>Met index : ${activity.met_index}
     <br>Doba trvání: ${activity.duration} minut 
     <br>`;

  let createdDiv = document.createElement('div');
  createdDiv.classList.add('content_inline');

  let removeButton = document.createElement('button');
  createdDiv.appendChild(removeButton);
  removeButton.classList.add('content-button');
  removeButton.innerText = 'Smazat';
  removeButton.style.pointerEvents = 'auto';

  removeButton.addEventListener(
    'click',
    function () {
      _user._performed_activities = _user._performed_activities.filter(
        (element) => element.activity_record_id !== activity.activity_record_id,
      );
      _user.saveUser();
      loadStatePage();
    }.bind(activity),
  );

  // EDITING ACTIVITY
  let createdInputDiv = document.createElement('div');
  createdInputDiv.classList.add('content_inline_flex_start');
  createdInputDiv.style.display = 'none';
  createdElement.appendChild(createdInputDiv);

  let createdInputLabel = document.createElement('label');
  createdInputLabel.innerText = 'Doba trvání [min]';
  createdInputDiv.appendChild(createdInputLabel);

  let createdInput = document.createElement('input');
  createdInput.type = 'number';
  createdInput.id = getUniqueID();
  createdInputDiv.appendChild(createdInput);

  let confirmButton = document.createElement('button');
  confirmButton.classList.add('arrow-button');
  confirmButton.innerText = 'OK';
  createdInputDiv.appendChild(confirmButton);
  confirmButton.addEventListener(
    'click',
    function () {
      if (!validateNumberInput(createdInput.id)) return;
      let editedActivity = activity;
      editedActivity.duration = parseFloat(createdInput.value);

      _user._performed_activities = _user._performed_activities.filter(
        (element) => element.activity_record_id !== activity.activity_record_id,
      );
      _user._performed_activities.push(editedActivity);
      _user.saveUser();
      loadStatePage();
    }.bind(activity),
  );

  let editButton = document.createElement('button');
  editButton.classList.add('content-button');
  createdDiv.appendChild(editButton);
  editButton.innerText = 'Upravit';
  editButton.style.pointerEvents = 'auto';
  editButton.addEventListener('click', function () {
    if (createdInputDiv.style.display === 'none') {
      createdInputDiv.style.display = 'initial';
      editButton.innerText = 'Zrušit';
    } else {
      createdInputDiv.style.display = 'none';
      editButton.innerText = 'Upravit';
    }
  });

  createdElement.appendChild(createdDiv);

  document.querySelector('#today-activities').appendChild(createdElement);
}

/*
    Renders meal's values into a new element but only for meal eaten today
*/

/**
 * Function renders element in state page box content, containing description
 * of a meal consumed by user in given day.
 *
 * @param meal Object containing information about meal record
 * @param day Date in 'dd/mm/yyyy' format
 */
function renderMeal(meal, day) {
  if (meal.date !== day) {
    return;
  }

  // load meal data
  for (let snapshot of mealsSnapshot) {
    if (snapshot.id === meal.meal_id) {
      let mealSnapshot = snapshot.data();
      let amount_type = mealSnapshot.amount;

      let createdElement = document.createElement('label');
      createdElement.classList.add('label-content');
      createdElement.classList.add('generated-content');
      createdElement.innerHTML = `<b style="font-size: 17px;">${mealSnapshot.name}</b><br>
          <br>S hodnotami na ${amount_type}:
          <br>Kalorie : ${mealSnapshot.calories} kcal
          <br>Bílkoviny : ${mealSnapshot.protein} g
          <br>Tuky : ${mealSnapshot.fat} g
          <br>Sacharidy : ${mealSnapshot.sugars} g
          <br><br>V množství: ${meal.amount} g
          <br>`;

      if (amount_type === '100g' || amount_type === '100ml') {
        amount_type = 100;
      } else {
        amount_type = 1;
      }

      switch (meal.meal_type) {
        case 'Snídaně': {
          document.querySelector('#today-meals-1').appendChild(createdElement);
          break;
        }
        case 'Svačina': {
          document.querySelector('#today-meals-2').appendChild(createdElement);
          break;
        }
        case 'Oběd': {
          document.querySelector('#today-meals-3').appendChild(createdElement);
          break;
        }
        case 'Odpolední svačina': {
          document.querySelector('#today-meals-4').appendChild(createdElement);
          break;
        }
        case 'Večeře': {
          document.querySelector('#today-meals-5').appendChild(createdElement);
          break;
        }
        default: {
          document.querySelector('#today-meals-3').appendChild(createdElement);
          break;
        }
      }

      // update daily income variables
      caloriesIncome += (mealSnapshot.calories * meal.amount) / amount_type;
      proteinIncome += (mealSnapshot.protein * meal.amount) / amount_type;
      fatIncome += (mealSnapshot.fat * meal.amount) / amount_type;
      sugarsIncome += (mealSnapshot.sugars * meal.amount) / amount_type;

      let createdDiv = document.createElement('div');
      createdDiv.classList.add('content_inline');

      let createdButton = document.createElement('button');
      createdDiv.appendChild(createdButton);
      createdButton.classList.add('content-button');
      createdButton.innerText = 'Smazat';
      // Adds eventListener for delete button "click" event.
      // Removes meal element and subtracts nutrition values from stats.
      createdButton.addEventListener(
        'click',
        function () {
          _user._eaten_meals = _user._eaten_meals.filter(
            (element) => element.meal_record_id !== meal.meal_record_id,
          );
          _user.saveUser();
          loadStatePage();
        }.bind(meal),
      );

      // EDITING MEAL
      let createdInputDiv = document.createElement('div');
      createdInputDiv.classList.add('content_inline_flex_start');
      createdInputDiv.style.display = 'none';
      createdElement.appendChild(createdInputDiv);

      let createdInputLabel = document.createElement('label');
      createdInputLabel.innerText = 'Množství: ';
      createdInputDiv.appendChild(createdInputLabel);

      let createdInput = document.createElement('input');
      createdInput.type = 'number';
      createdInput.id = getUniqueID();
      createdInputDiv.appendChild(createdInput);

      let confirmButton = document.createElement('button');
      confirmButton.classList.add('arrow-button');
      confirmButton.innerText = 'OK';
      createdInputDiv.appendChild(confirmButton);
      confirmButton.addEventListener(
        'click',
        function () {
          if (!validateNumberInput(createdInput.id)) return;
          _user._eaten_meals.forEach((element) => {
            if (element.meal_record_id === meal.meal_record_id) {
              element.amount = parseFloat(createdInput.value);
            }
          });
          _user.saveUser();
          loadStatePage();
        }.bind(meal),
      );

      let editButton = document.createElement('button');
      editButton.classList.add('content-button');
      createdDiv.appendChild(editButton);
      editButton.innerText = 'Upravit';
      editButton.style.pointerEvents = 'auto';
      editButton.addEventListener('click', function () {
        if (createdInputDiv.style.display === 'none') {
          createdInputDiv.style.display = 'initial';
          editButton.innerText = 'Zrušit';
        } else {
          createdInputDiv.style.display = 'none';
          editButton.innerText = 'Upravit';
        }
      });

      createdElement.appendChild(createdDiv);
      break;
    }
  }
}

function displayTodayMeals() {
  document.getElementById('today-meals-0').style.display = 'flex';
  document.getElementById('today-meals-1').style.display = 'flex';
  document.getElementById('today-meals-2').style.display = 'flex';
  document.getElementById('today-meals-3').style.display = 'flex';
  document.getElementById('today-meals-4').style.display = 'flex';
  document.getElementById('today-meals-5').style.display = 'flex';
}

function hideTodayMeals() {
  document.getElementById('today-meals-0').style.display = 'none';
  document.getElementById('today-meals-1').style.display = 'none';
  document.getElementById('today-meals-2').style.display = 'none';
  document.getElementById('today-meals-3').style.display = 'none';
  document.getElementById('today-meals-4').style.display = 'none';
  document.getElementById('today-meals-5').style.display = 'none';
}

// Renders all meals eaten on the specific day and creates buttons for browsing also the previous days meals.

/**
 * Function fully updates state page data including rendering all performed
 * activities and consumed meals for current day.
 *
 * @param day Day in 'dd/mm/yyyy' date format
 */
function renderTodayMealsAndActivities(day) {
  document.getElementById('date-control').innerHTML = ``;

  // Rendering activities
  document.getElementById('activities-checkbox').innerHTML = ``;
  document.getElementById('activities-checkbox').addEventListener('change', function () {
    if (this.checked) {
      document.getElementById('today-activities').style.display = 'flex';
    } else {
      document.getElementById('today-activities').style.display = 'none';
    }
  });
  document.getElementById('today-activities').innerHTML = '';

  // add new activity button
  let activitiesDiv = document.createElement('div');
  activitiesDiv.classList.add('content_inline_center');

  let activitiesLabel = document.createElement('label');
  activitiesLabel.innerText = 'Přidat aktivitu';
  activitiesLabel.style.display = 'flex';
  activitiesLabel.style.justifyContent = 'center';
  activitiesLabel.style.alignItems = 'center';

  let addActivityButton = document.createElement('button');
  addActivityButton.classList.add('arrow-button');
  addActivityButton.innerText = '+';
  addActivityButton.addEventListener('click', () => {
    window.location.hash = '#activity';
    document
      .getElementById('search-activity-input')
      .scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    document.getElementById('search-activity-input').focus({ preventScroll: false });
  });

  activitiesDiv.appendChild(activitiesLabel);
  activitiesDiv.appendChild(addActivityButton);
  document.getElementById('today-activities').appendChild(activitiesDiv);

  // Rendering meals
  document.getElementById('meals-checkbox').innerHTML = ``;
  document.getElementById('meals-checkbox').addEventListener('change', function () {
    if (this.checked) {
      displayTodayMeals();
    } else {
      hideTodayMeals();
    }
  });

  // add new meal button
  let mealsDiv = document.createElement('div');
  mealsDiv.classList.add('content_inline_center');

  let mealsLabel = document.createElement('label');
  mealsLabel.innerText = 'Přidat jídlo';
  mealsLabel.style.display = 'flex';
  mealsLabel.style.justifyContent = 'center';
  mealsLabel.style.alignItems = 'center';

  let addMealButton = document.createElement('button');
  addMealButton.classList.add('arrow-button');
  addMealButton.innerText = '+';
  addMealButton.addEventListener('click', () => {
    window.location.hash = '#meals';
    document
      .getElementById('search-meal-input')
      .scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    document.getElementById('search-meal-input').focus({ preventScroll: false });
  });

  mealsDiv.appendChild(mealsLabel);
  mealsDiv.appendChild(addMealButton);

  document.getElementById('today-meals-0').innerHTML = '';
  document.getElementById('today-meals-0').appendChild(mealsDiv);
  document.getElementById('today-meals-1').innerHTML = 'Snídaně: ';
  document.getElementById('today-meals-2').innerHTML = 'Svačina: ';
  document.getElementById('today-meals-3').innerHTML = 'Oběd: ';
  document.getElementById('today-meals-4').innerHTML = 'Odpolední svačina: ';
  document.getElementById('today-meals-5').innerHTML = 'Večeře: ';

  let dateControl = document.getElementById('date-control');

  let leftArrowImage = new Image();
  leftArrowImage.src = 'images/left.png';
  leftArrowImage.alt = 'left-arrow-image';
  leftArrowImage.style.marginTop = '4px';
  leftArrowImage.style.marginRight = '2px';

  let createdButtonLastDay = document.createElement('button');
  createdButtonLastDay.classList.add('arrow-button');
  createdButtonLastDay.name = 'last-day';
  createdButtonLastDay.style.borderRight = '40px blue';
  createdButtonLastDay.appendChild(leftArrowImage);
  createdButtonLastDay.addEventListener('click', () => {
    let d = todayDate;
    d.setDate(d.getDate() - 1);
    let dd = String(d.getDate()).padStart(2, '0');
    let mm = String(d.getMonth() + 1).padStart(2, '0');
    let yyyy = d.getFullYear();
    d = dd + '/' + mm + '/' + yyyy;
    renderTodayMealsAndActivities(d);
  });

  let dateLogo = new Image();
  dateLogo.src = 'images/date-logo.png';
  dateLogo.alt = 'date-logo';
  dateLogo.height = 20;
  dateLogo.width = 20;
  dateLogo.style.marginTop = 'auto';
  dateLogo.style.marginBottom = 'auto';

  let createdLabel = document.createElement('label');
  const parsedDay = day.replace(/\//g, '.');
  createdLabel.style.display = 'flex';
  createdLabel.innerText = parsedDay;
  createdLabel.style.alignItems = 'center';
  createdLabel.style.color = 'grey';

  let rightArrowImage = new Image();
  rightArrowImage.src = 'images/right.png';
  rightArrowImage.alt = 'right-arrow-image';
  rightArrowImage.style.marginTop = '4px';
  rightArrowImage.style.marginLeft = '2px';

  let createdButtonNextDay = document.createElement('button');
  createdButtonNextDay.classList.add('arrow-button');
  createdButtonNextDay.name = 'next-day';
  createdButtonNextDay.appendChild(rightArrowImage);
  createdButtonNextDay.addEventListener('click', () => {
    let d = todayDate;
    d.setDate(d.getDate() + 1);
    let dd = String(d.getDate()).padStart(2, '0');
    let mm = String(d.getMonth() + 1).padStart(2, '0');
    let yyyy = d.getFullYear();
    d = dd + '/' + mm + '/' + yyyy;
    renderTodayMealsAndActivities(d);
  });

  dateControl.appendChild(createdButtonLastDay);
  dateControl.appendChild(dateLogo);
  dateControl.appendChild(createdLabel);
  dateControl.appendChild(createdButtonNextDay);

  caloriesIncome = 0;
  proteinIncome = 0;
  fatIncome = 0;
  sugarsIncome = 0;

  // Render all activites by specific day.
  for (let activity of _user._performed_activities) {
    renderActivity(activity, day);
  }

  // Render all meals by specific day.
  for (let meal of _user._eaten_meals) {
    renderMeal(meal, day);
  }

  loadPieChartData(day);
}

/**
 * Function loads state page with user stats and updates content accordingly.
 */
function loadStatePage() {
  clearContentBoxes();
  renderTodayMealsAndActivities(today);
  highlightLink('state-link');
  loadContentBoxes('state-boxes');
  updateArticle(
    'Denní přehled',
    'Zde nalezneš přehled svého denního kalorického příjmu a výdeje včetně zapsaných jídel a aktivit.',
  );
  pageLogoImg.src = 'images/prehled.png';
}

/**
 * Function loads user data from database and updates user snapshot.
 * @returns {Promise<void>}
 */
async function loadUserDataOnce() {
  try {
    const response = await db.collection('users').doc(userAuth.uid).get();
    userSnapshot = response.data();
    _user = new User(
      userSnapshot.name,
      userSnapshot.weight,
      userSnapshot.height,
      userSnapshot.year_of_birth,
      userSnapshot.sex,
      userSnapshot.activity,
      userSnapshot.goal,
      userSnapshot.eaten_meals,
      userSnapshot.performed_activities,
    );
    displayDropdown();
    updateUserBox();
    let params = getParams(window.location.href);
    if (params.meal_id != null) {
      loadCreatedMealPage(params.meal_id);
    } else {
      window.location.hash = '#state';
      loadStatePage();
    }
  } catch (error) {
    console.log('Error occurred when loading user data: ' + error);
    window.location.hash = '#logout';
  }
}

/**
 * Function sets listener on login / logout from app.
 */
function setAuthOnStatusChange() {
  auth.onAuthStateChanged((user) => {
    if (user) {
      userAuth = user;
      document.getElementById('nav-menu').classList.add('hide_auth');
      document.getElementById('nav-menu').classList.remove('show_auth');
      document.getElementById('user-content').classList.remove('before-login');
      document.getElementById('user-content').classList.add('after-login');

      // load user data
      loadUserDataOnce();
    } else {
      userAuth = null;
      document.getElementById('nav-menu').classList.add('show_auth');
      document.getElementById('nav-menu').classList.remove('hide_auth');
      document.getElementById('user-content').classList.add('before-login');
      document.getElementById('user-content').classList.remove('after-login');
      document.title = 'Přihlášení';
      loadLoginPage();
      hideDropdown();
    }
  });
}

/**
 * Function sets listener on 'meals' collection in Firestore. When collection
 * is updated 'onSnapshot' function is triggered and updates mealsSnapshot.
 */
function setMealsDatabaseListener() {
  db.collection('meals').onSnapshot(
    function (querySnapshot) {
      mealsSnapshot = [];
      querySnapshot.forEach(function (doc) {
        mealsSnapshot.push(doc);
      });
    },
    (error) => {
      console.log(error);
    },
  );
}

/**
 * Function sets listener on 'activities' collection in Firestore. When collection
 * is updated 'onSnapshot' function is triggered and updates activitiesSnapshot.
 */
function setActivitiesDatabaseListener() {
  db.collection('activities').onSnapshot(
    function (querySnapshot) {
      activitiesSnapshot = [];
      querySnapshot.forEach(function (doc) {
        activitiesSnapshot.push(doc);
      });
    },
    (error) => {
      console.log(error);
    },
  );
}

function setCheckboxesChecked() {
  document.getElementById('meals-checkbox').checked = true;
  document.getElementById('activities-checkbox').checked = true;
}

/**
 * Function loads page with sign up content boxes.
 */
function loadSignUpPage() {
  clearContentBoxes();
  highlightLink('signup-link');
  loadContentBoxes('signup-boxes');
  updateArticle('Registrace uživatele', 'Zaregistruj se do aplikace.');
  pageLogoImg.src = 'images/user_login.png';
}

/**
 * Function loads meals page for searching meals and updates content accordingly
 */
function loadMealsPage() {
  clearContentBoxes();
  highlightLink('meals-link');
  loadContentBoxes('meals-boxes');
  updateArticle(
    'Hledání a zápis potravin',
    'Vyhledaná jídla si lze zapsat do příjmu, nebo přidat k nově vytvářenému pokrmu ze sekce Nové jídlo.',
  );
  pageLogoImg.src = 'images/search.png';
  document.getElementById('search-meal-content-all').innerHTML = 'Výsledky hledání: ';
  scanning_start_button.innerText = 'Start';
  scanning_start_button.style.backgroundColor = '#2fd015';
}

/**
 * Function loads settings page and updates content accordingly.
 */
function loadSettingsPage() {
  clearContentBoxes();
  highlightLink('settings-link');
  loadContentBoxes('settings-boxes');
  updateArticle(
    'Nastavení uživatelských údajů',
    'Vyplň tabulku pro výpočet bazálního metabolismu a denního kalorického výdeje a zjisti tak svůj doporučený denní kalorický příjem.',
  );
  pageLogoImg.src = 'images/settings.png';
}

/**
 * Function loads new meal page and updates page content accordingly.
 */
function loadNewMealPage() {
  clearContentBoxes();
  highlightLink('newmeal-link');
  loadContentBoxes('home-boxes');
  updateArticle(
    'Tvorba nového pokrmu',
    'Vyplň údaje o jídle a ulož jej do databáze aplikace. Po uložení bude vygenerován QR kód pro sdílení informací o pokrmu. ' +
      'K vytvoření nového jídla lze použít i již existující pokrmy.',
  );
  pageLogoImg.src = 'images/cake.png';
}

/**
 * Function loads page content accordingly to 'window.location.hash'.
 * @param hash Value of currently displayed page's hash.
 */
function pageController(hash) {
  document.getElementById('menu-control-input').checked = false;
  resetLinksHighlights();
  if (userAuth) {
    // turn off scanning
    if (isScanning) stopScanning();
    // reset page position
    window.scrollTo(0, 0);
    // load page with given hash
    switch (hash) {
      case '#newmeal': {
        document.title = 'Nové jídlo';
        loadNewMealPage();
        break;
      }
      case '#activity': {
        loadActivityPage();
        document.title = 'Aktivity';
        break;
      }
      case '#meals': {
        loadMealsPage();
        document.title = 'Hledání potravin';
        break;
      }
      case '#settings': {
        loadSettingsPage();
        document.title = 'Nastavení uživatele';
        break;
      }
      case '#logout': {
        highlightLink('logout-link');
        auth.signOut();
        break;
      }
      case '#created_meal': {
        document.title = 'Přehled potraviny';
        break;
      }
      case '#state': {
        loadStatePage();
        document.title = 'Denní přehled';
        break;
      }
    }
  } else {
    switch (hash) {
      case '#signup': {
        loadSignUpPage();
        document.title = 'Registrace';
        break;
      }
      default:
        loadLoginPage();
        document.title = 'Přihlášení';
        break;
    }
  }
}

/**
 * Function displays offline bar when there is no internet connection.
 * After successful reconnect offline bar updates.
 */
function updateOnlineStatus() {
  let offlineBar = document.getElementById('offline-bar');
  if (navigator.onLine) {
    offlineBar.style.backgroundColor = 'green';
    offlineBar.innerText = 'Online';
    window.setTimeout(
      function () {
        offlineBar.style.display = 'none';
      }.bind(offlineBar),
      2000,
    );
  } else {
    offlineBar.innerText = 'Offline';
    offlineBar.style.backgroundColor = 'red';
    offlineBar.style.display = 'flex';
  }
}

/**
 * Function registers Service Worker script and initializes today variable to
 * today's date in 'dd/mm/yyyy' format.
 * Loads stats and meals variables and adds eventListener to 'popstate' event
 * for changing page and loads home page.
 */
function loadPage() {
  // register sw.js script
  navigator.serviceWorker.register('/sw.js').then(
    function (registration) {
      console.log(
        'ServiceWorker registration successful with scope: ',
        registration.scope,
      );
    },
    function (err) {
      console.log('ServiceWorker registration failed: ', err);
    },
  );
  // load current date
  let dd = String(todayDate.getDate()).padStart(2, '0');
  let mm = String(todayDate.getMonth() + 1).padStart(2, '0');
  let yyyy = todayDate.getFullYear();
  today = dd + '/' + mm + '/' + yyyy;
  // clear all content boxes
  clearContentBoxes();
  // clear new meal inputs
  clearNewMealPage();
  // set checkboxes
  setCheckboxesChecked();
  // set page location hash
  window.addEventListener('popstate', () => {
    pageController(window.location.hash);
  });
  // set online status listener
  window.addEventListener('online', updateOnlineStatus);
  window.addEventListener('offline', updateOnlineStatus);
  // change page location
  window.location.hash = '#state';
}

/**
 * Function add listeners to input element and generates results of fulltext
 * searched meals based on user's latest input.
 *
 * @param inputId Input element id.
 * @param outputId Output div element id.
 */
function autocomplete(inputId, outputId) {
  let searchInput = document.getElementById(inputId);
  let outputElement = document.getElementById(outputId);
  let records;

  searchInput.addEventListener('input', () => {
    records = [];
    outputElement.innerHTML = '';
    if (searchInput.value < 1) return;

    // create array of normalised strings from input value
    let normalisedInputString = searchInput.value
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .toLowerCase();
    let inputStrings = normalisedInputString.trim().split(' ');

    // convert all meals names to arrays of strings for better querying
    for (let doc of mealsSnapshot) {
      let rawString = doc.data().name;
      let normalisedString = rawString
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .toLowerCase();
      let arrayOfStrings = normalisedString.trim().split(' ');

      for (let subString of inputStrings) {
        if (
          arrayOfStrings.some(
            (keyword) => keyword.startsWith(subString) && keyword !== '-',
          )
        ) {
          records.push({ id: doc.id, data: doc.data() });
          break;
        }
      }
    }
    let resultNum = 0;
    for (let record of records) {
      resultNum++;
      if (resultNum === 6) break;

      let createdElement = document.createElement('a');
      createdElement.innerText = record.data.name;
      createdElement.style.cursor = 'pointer';
      createdElement.addEventListener('click', () => {
        loadCreatedMealPage(record.id);
      });

      outputElement.appendChild(createdElement);
    }
  });

  searchInput.addEventListener('focusout', () => {
    window.setTimeout(() => {
      searchInput.value = '';
      outputElement.innerHTML = '';
    }, 150);
  });
}

/**
 * This function adds listeners on checkboxes for hiding / displaying passwords.
 */
function addPasswordsCheckboxesListeners() {
  const regPasswordCheckbox = document.getElementById('reg-password-checkbox');
  const regPasswordInput = document.getElementById('reg_password');
  regPasswordCheckbox.addEventListener('change', function () {
    if (this.checked) {
      regPasswordInput.type = 'text';
    } else {
      regPasswordInput.type = 'password';
    }
  });
  const loginPasswordCheckbox = document.getElementById('login-password-checkbox');
  const loginPasswordInput = document.getElementById('login_password');
  loginPasswordCheckbox.addEventListener('change', function () {
    if (this.checked) {
      loginPasswordInput.type = 'text';
    } else {
      loginPasswordInput.type = 'password';
    }
  });
}

// add button listeners
initializeAllButtonListeners();

// add checkboxes listeners
addPasswordsCheckboxesListeners();

// add auth listeners
setAuthOnStatusChange();

// add database listeners
setMealsDatabaseListener();
setActivitiesDatabaseListener();

// set autocomplete listeners
autocomplete('header-search', 'searched-records');

// set window.onload function
window.onload = loadPage;
