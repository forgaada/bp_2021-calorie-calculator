let CACHE_NAME = 'nutrition-tracker-v1';
let DYNAMIC_CACHE_NAME = 'nutrition-tracker-dynamic-v1';
let urlsToCache = [
  '/',
  '/index.html',
  '/styles/styles.css',
  '/bundle.js',
  '/images/logo.png',
  '/images/pears.jpg',
  '/images/user_login.png',
  '/404.html',
];

// install service worker
self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
      return cache.addAll(urlsToCache);
    }),
  );
});

// activate event
self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then((keys) => {
      return Promise.all(
        keys
          .filter((key) => key !== CACHE_NAME && key !== DYNAMIC_CACHE_NAME)
          .map((key) => caches.delete(key)),
      );
    }),
  );
});

// fetch event listener
self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches
      .match(event.request)
      .then((cacheResponse) => {
        return (
          cacheResponse ||
          fetch(event.request).then((fetchResult) => {
            return caches.open(DYNAMIC_CACHE_NAME).then((cache) => {
              cache
                .put(event.request.url, fetchResult.clone())
                .then()
                .catch((err) => {
                  console.log(err);
                });
              return fetchResult;
            });
          })
        );
      })
      .catch(() => {
        if (event.request.url.indexOf('.html') > -1) {
          return caches.match('/404.html');
        }
      }),
  );
});
