# Nutrition Tracker

> Progressive Web Application for Caloric Intake and Expenditure Management

Tento projekt slouží jako repozitář pro tvorbu webové aplikace v rámci předmětu BBAP20.

Nasazená webová aplikace je dostupná na adrese: [https://calory-calculator-13.web.app/](https://calory-calculator-13.web.app/) .

Přihlašovací údaje: login: admin@email.cz / password: admin123

## Spuštění aplikace

Pro instalaci je nutné mít stažený správce balíčků npm (Node Package Manager).

``` bash
# install dependencies
$ npm install

# create bundle.js file from modules ( run after each application code change )
$ rollup --config

Open /public/index.html file to run app in a browser.

```

## Spuštění automatizovaných Cypress testů

Pro spuštění testů je rovněž nutné mít nainstalovaný npm a zavolaný příkaz `$ npm install`.

``` bash
# run tests in terminal via Electron
$ npx cypress run

# run tests in graphic mode with selected browser
$ npx cypress open

```



