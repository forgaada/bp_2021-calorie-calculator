module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
    'prettier',
    'plugin:prettier/recommended',
    'plugin:cypress/recommended',
    'plugin:chai-friendly/recommended',
    'plugin:import/warnings',
    'plugin:import/errors',
    'plugin:import/typescript',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint',
    'only-warn',
    'prettier',
    'cypress',
    'chai-friendly',
    'import',
  ],
  rules: {
    'no-underscore-dangle': ['error', { allowAfterThis: true }],
    // these rules override airbnb standard to fit WebStorm and Cypress
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        ts: 'never',
      },
    ],
    'class-methods-use-this': 'off', // default rules do not work with Cypress Page Object classes
  },
};
