context('Test creating, searching and recording meals', () => {
  beforeEach(() => {
    cy.login();
  });

  it('Should successfully find and record meal', () => {
    const correctFinalUrl = 'https://calory-calculator-13.web.app/#state';
    cy.get('[id="header-search"]').type('Coca Cola');
    cy.contains('Coca Cola').click();
    cy.get('[id="created-meal-amount-input"]').type('1000');
    cy.get('[id="record-meal-button"]').click();

    // assert that page was loaded properly
    cy.url().should('eq', correctFinalUrl);

    // take a screenshot
    cy.screenshot({ capture: 'runner' });

    // logout from app
    cy.logout();
  });

  it('Should successfully create new meal', () => {
    const correctFinalUrl = 'https://calory-calculator-13.web.app/#created_meal';

    cy.contains('Tvorba pokrmu').click();
    cy.wait(4000);
    cy.get('[id="meal-name-input"]').type('Test', { force: true });
    cy.get('[id="meal-calories-input"]').type('100', { force: true });
    cy.get('[id="meal-protein-input"]').type('20', { force: true });
    cy.get('[id="meal-fat-input"]').type('10', { force: true });
    cy.get('[id="meal-sugars-input"]').type('5', { force: true });
    cy.get('[id="meal-ean-input"]').type('111000333', { force: true });
    cy.get('[id="meal_button"]').click({ force: true });
    cy.wait(2000);

    // assert that page was loaded properly
    cy.url().should('eq', correctFinalUrl);

    // take a screenshot
    cy.screenshot({ capture: 'runner' });

    // delete created meal
    cy.get('[id="delete-meal-button"]').click({ force: true });

    // logout from app
    cy.logout();
  });
});
