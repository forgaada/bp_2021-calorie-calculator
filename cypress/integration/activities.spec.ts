context('Test recording activities', () => {
  beforeEach(() => {
    cy.login();
  });

  it('Should successfully add activity record to daily outcome', () => {
    const correctFinalUrl = 'https://calory-calculator-13.web.app/#state';

    cy.contains('Zapsat aktivitu').click();
    cy.wait(4000);
    cy.get('[id="search-activity-input"]').type('Powerlifting', { force: true });
    cy.get('[id="search-activity-button"]').click({ force: true });
    cy.get('[id="Powerlifting-add-button"]').click({ force: true });
    cy.get('[id="Powerlifting-input"]').type('60', { force: true });
    cy.get('[id="Powerlifting-confirm-button"]').click({ force: true });

    // assert that page was loaded properly
    cy.url().should('eq', correctFinalUrl);

    // take a screenshot
    cy.screenshot({ capture: 'runner' });

    // logout from app
    cy.logout();
  });
});
