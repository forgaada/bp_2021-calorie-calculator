context('Test updating user settings', () => {
  beforeEach(() => {
    cy.login();
  });

  it('Should successfully update information about user', () => {
    const correctFinalUrl = 'https://calory-calculator-13.web.app/#state';

    cy.contains('Nastavení').click();
    cy.wait(4000);
    cy.get('[id="name-input"]').type('Admin', { force: true });
    cy.get('[id="weight-input"]').type('84', { force: true });
    cy.get('[id="height-input"]').type('185', { force: true });
    cy.get('[id="year-of-birth-input"]').type('1999', { force: true });
    cy.get('[id="basal_button"]').click({ force: true });

    // assert that page was loaded properly
    cy.url().should('eq', correctFinalUrl);

    // take a screenshot
    cy.screenshot({ capture: 'runner' });

    // logout from app
    cy.logout();
  });
});
