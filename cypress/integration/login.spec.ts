context('Test login to app', () => {
  it('Should successfully login user to app', () => {
    const correctFinalUrl = 'https://calory-calculator-13.web.app/#state';

    cy.login();

    // assert that page was loaded properly
    cy.url().should('eq', correctFinalUrl);

    // take a screenshot
    cy.screenshot({ capture: 'runner' });

    // log out from app
    cy.logout();
  });
});
