/* eslint-disable no-unused-vars */
declare namespace Cypress {
  interface Chainable {
    // Authentication
    login(): void;
    logout(): void;
  }
}

const login = (): void => {
  cy.visit('#login');
  cy.get('[id="login_mail"]').type('admin@email.cz');
  cy.get('[id="login_password"]').type('admin123');
  cy.get('[id="login_button"]').click();
  cy.wait(2000);
};

const logout = (): void => {
  cy.contains('Odhlásit').click();
  cy.wait(2000);
};

Cypress.Commands.add('login', login);
Cypress.Commands.add('logout', logout);
